=====================================
 Enabling DevOps for VNF development
=====================================
:Author: `Erik Schilling <mail@ablu.org>`_
:Date: 2018-10-17


Motivation
==========

VNFs
----
.. image:: figures/vnfs.svg
    :width: 100%
- Replace special hardware
- Simple functions chained to form complex scenarios
- More flexibility

------------------------------

.. image:: figures/osm-logo.svg
    :width: 80%
    :align: center

`**O**pen **S**ource **M**anagement **a****n**d **O**rchestration <https://osm.etsi.org/>`_

- Implements ETSI specifications for VNFs
- Collaboration of industry and research


OSM - VNFD
----------
.. class:: columns

    .. class:: column
        :width: 40%

    .. code:: yaml

        vdu:
          vm-flavor:
            vcpu-count: 1
            image: cirros034
          interface:
          - name: eth0
        connection-point:
        - name: eth0

    .. class:: column
        :width: 65%

        **V**irtual **N**etwork **F**unction **D**efinition

        - Greatly simplified here
        - Defines source image and constraints
        - Exports connection points
        - Not deployable on its own

OSM - NSD
---------
.. class:: columns

    .. class:: column
        :width: 50%

    .. code:: yaml

        constituent-vnfd:
        - member-vnf-index: 1
          vnfd-id-ref: cirros_vnfd
        # ...
        vld:
        - member-vnf-index-ref: 1
          vnfd-connection-point-ref: eth0
        # ...

    .. class:: column
        :width: 50%

        **N**etwork **S**ervice **D**efinition

        - Aggregates individual VNFDs to complex services
        - Deployable on its own

Architecture
------------
.. image:: figures/architecture.svg
    :width: 100%

`Juju Charms <https://jujucharms.com/>`_
----------------------------------------
.. image:: figures/juju-charms.svg
    :width: 100%

- Abstraction for low-level management tasks
- Exports set of actions and states

DevOps ready?
-------------
.. image:: figures/architecture.svg
    :width: 100%

vim-emu
-------
.. image:: figures/architecture-vim-emu.svg
    :width: 100%

vim-emu
-------
.. code:: python

    self.net = DCNetwork()
    self.dc1 = self.net.addDatacenter("dc1")
    self.dc2 = self.net.addDatacenter("dc2")
    self.net.addLink(self.dc1, self.dc2,
                     cls=TCLink, delay="50ms")
    self.api1 = OpenstackApi("0.0.0.0", 6001)
    self.api1.connect_datacenter(self.dc1)
    self.api2 = OpenstackApi("0.0.0.0", 6002)
    self.api2.connect_datacenter(self.dc2)

vim-emu
-------
- Based on `Containernet <https://containernet.github.io/>`_ which is based on `Mininet <http://mininet.org/>`_
- Can emulate 100's of OpenStack installations on developer hardware

Problems
========

Problems
--------

.. class:: columns

    .. class:: column
        :width: 80%

    - ``vim-emu``'s network emulation is lacking
      - Only fixed Floating IPs
      - No actual routing of Floating IPs
      - Service function chaining API
    - LXC based Juju Charms have no access to isolated environment

    .. class:: column
        :width: 20%

    .. image:: figures/exclamation.svg
        :width: 100%

Floating-IPs
------------
.. image:: figures/floating-ips.svg
    :width: 100%

Floating-IPs reality
--------------------
.. image:: figures/floating-ips-real.svg
    :width: 100%

Service Function Chaining API
-----------------------------
.. image:: figures/service-function-chaining.svg
    :width: 100%

- Currently not implemented
- Requires mocking of `OpenStack API <https://docs.openstack.org/networking-sfc/queens/>`_

Isolated environment
--------------------
.. image:: figures/isolated-environment.svg
    :width: 100%


Solution Ideas
==============

Isolated environment Solution 1
-------------------------------
.. image:: figures/isolated-environment-fixed.svg
    :width: 100%

Isolated environment Solution 2
-------------------------------
.. image:: figures/isolated-environment-merged.svg
    :width: 100%

Comparison
-----------
.. class:: columns

    .. class:: column

        .. image:: figures/isolated-environment-fixed.svg
            :width: 100%
        - Simple
        - Does not allow realistic model
        - Allows testing with existing environments

    .. class:: column

        .. image:: figures/isolated-environment-merged.svg
            :width: 100%
        - More complex
        - Allows somewhat realistic models
        - Can reconfigure internals automatically

Comparison continued...
-----------------------
.. class:: columns

    .. class:: column

        .. image:: figures/isolated-environment-fixed.svg
            :width: 100%
        - Testing existing installations is a strong use case

    .. class:: column

        .. image:: figures/isolated-environment-merged.svg
            :width: 100%
        - Automated distributed OSM installs would be possible
. . .

**One solution does not exclude the other!**


Goal
====

Goal
----
- **Upstream work**
- Work in collaboration with the OSM developers
- Allow testing of (more) complex VNF chains

.. image:: figures/devops-cycle.svg
    :width: 100%

Evaluation
----------
- Performance evaluation of OSM and vim-emu with (very) long VNF chains
- Effect of topology changes on chains and charms
- Analysis of distributed OSM installations
- Comparability with real world scenarios

Schedule
--------
====== =============
week   task
====== =============
1-4:   Solution candidate, related projects, testbed
5-6:   Picking solution, defining use cases
7-11:  Implementation of solution candidate
12-15: Evaluation, Upstreaming of work
16-18: Polish work, Upstreaming buffer
19-20: Proof-reading, Buffer
====== =============

Outline
-------

.. class:: columns

    .. class:: column
        :width: 80%

    - Motivation / Introduction
    - Goal / Use Cases
    - Related work
    - Design
    - Implementation
    - Analysis / Testing of implementation
    - Future Work
    - Conclusion

    .. class:: column
        :width: 20%

    .. image:: figures/outline.svg
        :width: 100%



Discussion
==========

Title Ideas
-----------
- Enabling DevOps for VNF development
- Offline VNF Testing
- Virtualization of Virtual Network Functions

Thanks to
---------
- Manuel Peuster for supervision
- https://github.com/Xaviju/inkscape-open-symbols for icons