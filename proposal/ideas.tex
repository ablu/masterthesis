\section{Problems and solution ideas}
\label{sec:ideas}

This section is supposed to give some general ideas about the problems and potential solutions which could fulfill the goal.
These solutions described here solve different aspects of the full goal and will need to be analyzed in further detail.
The final solution will likely be a combination of different candidates but might also include completely different ideas which may occur during this thesis.


\subsection{Implementation of networking \ac{API}}
As of right now, \texttt{vim-emu} only provides a very basic implementation of the OpenStack networking \ac{API}.

\subsubsection{Service Function Chaining API is missing}
The most common use case of \acp{VNF} is to receive data on one interface, process it and forward it to another interface.
The orchestration mechanism then has to connect the individual \acp{VNF} in order to form a \ac{NS}.
While OpenStack has support for such an \ac{API}, \texttt{vim-emu} does not implement the relevant endpoints in order to automatically link \acp{VNF} together.

Here, the existing foundational work has to be finished.


\subsubsection{Floating-IP handling is only partly implemented}
The current implementation only returns a hard-coded IP range as available and does not keep track of assigned IPs nor actually makes the associated services available under assigned IPs. This leads to \ac{OSM} assigning the same IP to multiple instances, which in their turn, are then not reachable by configuration Charms because packets sent to the IP arrive nowhere.

The solution would consist of maintaining a proper list of available Floating-IPs and ensuring that traffic is actually routed to the assigned service.

However, since the \ac{OSM} services (Juju~Charms mostly) do not run inside of the emulated environment, \texttt{vim-emu} is then required to add routes to the emulated services which are not matchable to the emulated data-center. Thus, all management traffic would not be subject to the restrictions which are defined in the emulation script.

Resolving this situation would require to add additional ways to issue restrictions for traffic flowing over these Floating-IPs which, while technically possible, would be hard to model since it does not match the real-world network topologies.
Additionally, this solution still requires an additional layer above the \texttt{vim-emu} infrastructure for running the \ac{LXC} containers.
This makes the isolation and reproducibility of the environment harder.


\subsection{Integrating non-dockerized dependencies into \texttt{vim-emu}}
While making the services maintained by \texttt{vim-emu} available to the surrounding environment solves connectivity issues, it still does not allow to define restrictions for these outside facing links.
This may be solvable by moving the non-dockerized dependencies into \texttt{vim-emu} in order to leverage the existing topology description mechanisms.
There are many options to achieve this, each of them has their own benefits and issues:

\begin{enumerate}
    \item Configure Juju with the ``manual'' provider\footnote{\url{https://docs.jujucharms.com/1.25/en/config-manual}}.
    This requires manual provision of machines, which could be done as Docker container\footnote{Proof of concept can be found at: \url{https://gitlab.com/ablu/masterthesis/blob/master/lxc-analysis/Vagrantfile}}.
    This means that the machines could be controlled using the existing mechanisms for Docker containers in \texttt{vim-emu}.
    However, since Juju usually starts new machines for each Charm, the current implementation would need to be modified in order to tell Juju to use the pre-configured machine.

    \item Configure Juju with the ``MaaS'' provider\footnote{\url{https://docs.jujucharms.com/1.25/en/config-maas}}.
    It might be possible to configure MaaS as a Docker service and overriding the launch logic in order to start Docker containers.
    If that proves to be not viable, it still might be possible to implement a fake MaaS \ac{API} as part of \texttt{vim-emu} in order to then launch Docker containers instead of allocating physical hardware.

    \item Configure Juju with the ``OpenStack'' provider\footnote{\url{https://docs.jujucharms.com/1.25/en/config-openstack}} and point it at the already existing fake endpoints.
    The existing endpoints would then automatically try to launch Docker containers for providing the Charms.
    Here, however, Juju will then attempt to provision the newly launched containers as virtual machines in ways which are out of control of \texttt{vim-emu}. But, it might be possible to structure the launched containers in a way that they can deal with those provision attempts.

    \item Configure \texttt{lxd} with \texttt{vim-emu} restrictions.
    At least the networking restrictions of \texttt{vim-emu} could be emulated by manually configuring the \texttt{lxd} network.
    Resources such as CPU, RAM or disk storage would not be controllable, but that may also not be necessary since \ac{OSM} currently does not allow to define those restrictions for Juju~Charms either.
    Thus, this might be the most simple solution.

    \item Exchange the N2VC component of \ac{OSM}.
    The responsible component for launching Juju Charms is encapsulated into a standalone module\footnote{\url{https://osm.etsi.org/gitweb/?p=osm/N2VC.git;a=summary}} in the \ac{OSM} architecture.
    It would be possible to exchange this component in order to be able to perform additional configuration steps between the launch of the Charm and the actual execution of primitives.
    However, the code of the Juju interaction is 800+ lines long and thus would probably require non-neglectable ongoing maintenance effort.
\end{enumerate}

\subsection{Advanced isolation and reproducability}
The \ac{OSM} application itself is executed by a series of Docker services. Since \texttt{Containernet} (and thus \texttt{vim-emu}) already supports launching services as Docker containers, it might be viable to run the whole \ac{OSM} service inside of the emulated environment.
This would naturally allow simple configuration of the \ac{OSM} environment using the topology scripts and the \ac{OSM} controller would no longer be another component which requires maintenance, but be automatically configured and launched as well.

Next to more flexibility and realism of the emulated environment, it would also ease the development process of services.
A developer would only need to install \texttt{vim-emu} and use a script which defines the \ac{OSM} environment.
Manual configuration steps, like connecting the \ac{OSM} services with the (faked) OpenStack endpoints or provisioning \ac{OSM} with \acp{VNFD} and \ac{NSD} could be automated by the emulation script, simplifying repeated tests.

Since the definition of the \ac{OSM} services would then merely be a series of commands in the emulation script, testing complex setups with multiple, potentially differently versioned \ac{OSM} instances, would also be possible.

Another benefit would be a clean separation of the host network. If \ac{OSM} runs inside the emulated network it naturally receives access to the hosted services using the network emulation of \texttt{vim-emu}. Making services available to the outside world using special links would no longer be required.

The challenges of this solution mainly lie in the non-dockerized dependencies of \ac{OSM}. Namely, the Juju Charms, by default, depend on \ac{LXC} containers while \texttt{vim-emu} leverages Docker features for simulating data-center services.
However, it might also help with the solution of integration non-dockerized dependencies into \texttt{vim-emu}. If, for example, \ac{OSM} would have to be configured against a different \ac{LXD} endpoint, this could be automatically handled by \texttt{vim-emu} rather than requiring manual reconfiguration of the external \ac{OSM} install.


Additionally, both the \ac{OSM} and Juju community will be contacted in order to query alternative solutions or comments for solutions mentioned above.