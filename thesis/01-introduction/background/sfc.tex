\subsection{Service Function Chaining}
\label{sec:sfc}
Another technique which is commonly used in combination with \ac{OSM} is \acf{SFC}.
The term \ac{SFC} is defined by RFC7665~\cite{RFC7665} and covers the concept of transparently routing network packets through a chain of service functions depending on the packet metadata.

A common example for describing a service function is a firewall.
If a well-behaving client wants to connect to a server in order to query a resource, it should not care about a firewall being placed in between him and the server.
Also the server handles the traffic independently of the origin.
This means that the network has to ensure that the traffic of the client is filtered by the firewall before reaching the server.
Notable in this scenario is that the traffic returning from the server potentially does not need to be sent through the firewall again.
\ac{SFC} aims to allow such use cases by allowing to override the default path of a packet and routing it along a customized path instead.
In the \ac{NFV} environment this technique plays an important role for allowing to route specific traffic through \acp{VNF} or composing off-the-shelf \acp{VNF} to a specialized service.

While the general technique may also be applicable using lower-level techniques such as \ac{SDN}, the RFC defines a series of abstractions which are independent of the underlying data-plane or topology.
However, techniques like \ac{SDN} can greatly simplify the implementation of a \ac{SFC} architecture.

Since this work mostly deals with the OpenStack implementation\footnote{\url{https://docs.openstack.org/networking-sfc/latest/}} of \ac{SFC} a short introduction of the relevant topics is given here.

\paragraph{Setup:} The \ac{SFC} \ac{API} of OpenStack is still in a work-in-progress phase and thus requires manual installation and activation on top of an existing OpenStack install.
The process\footnote{\url{https://docs.openstack.org/networking-sfc/latest/install/index.html}} will not be repeated here, but mostly consists of installing the controller and agent extensions and enabling them in the respective configuration files.
The controller will provide the high-level \ac{API} while the agent will instantiate the defined rules depending on the lower-level networking plugin.
In this work, the reference implementation, which utilizes Open~vSwitch\footnote{\url{https://www.openvswitch.org/}} is used.

\paragraph{Port Pair:} The most low-level resource in the OpenStack \ac{SFC} \ac{API} is a port pair.
The resource combines the ingress and the egress port of a service function instance and thus defines a single hop within the Service~Function~Chain.

While OpenStack allows the creation of ports without binding them to an instance at creation time, a port pair requires a port to be bound to a function instance.
In practice this means that it is necessary to start a server instance associated with the port before creating the pair.
An example for this is given in \Cref{lst:sfc-portpair-creation}.

\begin{lstlisting}[caption={Example for creating a port pair. After creating the ports they are added to newly created server instances. Once the server instances are started it is possible to create the port pairs.}, label=lst:sfc-portpair-creation,float]
openstack port create p1 --network internal
openstack port create p2 --network internal
openstack port create p3 --network internal

openstack server create --port p1 --flavor m1.tiny --image cirros s1
openstack server create --port p2 --flavor m1.tiny --image cirros s2
openstack server create --port p3 --flavor m1.tiny --image cirros s3

openstack sfc port pair create --ingress p2 --egress p2 pp2
openstack sfc port pair create --ingress p3 --egress p3 pp3
\end{lstlisting}

As demonstrated in the example, the ingress and egress port of a port pair can be the same.
The port \texttt{p1} will be the source of the traffic in this example and thus does not require to be part of a port pair, but will later be matched using a classifier.

After executing the code above, OpenStack should have created the port pairs \texttt{pp1}, \texttt{pp2} and \texttt{pp3} which represent the service functions provided by the servers \texttt{s1}, \texttt{s2} and \texttt{s3} respectively.

\paragraph{Port Pair Group:} A port pair group can aggregate multiple port pairs to a single group.
This allows load-balancing over multiple service function instances of the same type.
Since in this example one server should represent an individual function, the port pair groups will consist of a single port pair each:

\begin{lstlisting}
openstack sfc port pair group create --port-pair pp2 ppg2
openstack sfc port pair group create --port-pair pp3 ppg3
\end{lstlisting}

\paragraph{Flow Classifier:} The flow classifier is required for matching the packets which should be routed along the chain of services.
Classifiers can match on the source or target port, the Layer 2 ethertype (IPv4 or IPv6), a protocol type (ICMP, TCP, ...), a source or target IP prefix or a source or target TCP or UDP port range.

\Cref{lst:sfc-flow-classifier-example} shows an example of a classifier which matches packets which arrive from port \texttt{p1} and target 10.0.0.12:80 TCP.

\begin{lstlisting}[caption={Example of the creation of a flow classifier which matches traffic originating from \texttt{p1} and targeting TCP port 80 of \texttt{10.0.0.12}.}, label=lst:sfc-flow-classifier-example,float]
openstack sfc flow classifier create \
    --logical-source-port p1 \
    --protocol TCP \
    --destination-ip-prefix 10.0.0.12 \
    --logical-destination-port 80 \
    fc1
\end{lstlisting}

\paragraph{Port Chain:} The final, missing step is to combine the port pair groups and flow classifiers to an actual forwarding rule.
This is done by creating a port chain as shown in \Cref{lst:sfc-port-chain-example}.
Here, an ordered list of port pair groups has to be specified next to the classifier.

\begin{lstlisting}[caption={Example for the creation of a port chain, consisting of \texttt{ppg2} and \texttt{ppg3} and the classifier \texttt{fc1}.}, label=lst:sfc-port-chain-example,float]
openstack sfc port chain create \
    --port-pair-group ppg2 \
    --port-pair-group ppg3 \
    --flow-classifier fc1 \
    pc1
\end{lstlisting}

As a result OpenStack will deploy rules to the forwarding tables of its virtual routers to forward any traffic matching the classifier \texttt{fc1} to be routed to an ingress port of the port pair group \texttt{ppg2} (\texttt{p2} in this example) and from the egress port of that group to an ingress port of \texttt{ppg3} (\texttt{p3} in this example).
After exiting an egress port of \texttt{ppg3} the special treatment ends and the default forwarding continues.
\Cref{fig:sfc} illustrates the resulting port chain.

\begin{figure}
    \centering
    \def\svgwidth{0.4\columnwidth}
    \input{figures/sfc.pdf_tex}
    \caption{Illustration of the resulting port chain \texttt{pc1}.}
    \label{fig:sfc}
\end{figure}

OpenStack's \ac{SFC} \ac{API} also supports some more higher-level features, which are not relevant for this work and therefore leaved out.
