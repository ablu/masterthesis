#!/bin/bash
set -xe

virt-builder centos-7.6 --root-password password:test \
    --install "epel-release" \
    --install "nc" \
    --install "iftop"  \
    --install "tcpdump"  \
    --append-line '/etc/systemd/system/iftop.service:[Unit]' \
    --append-line '/etc/systemd/system/iftop.service:Description=iftop' \
    --append-line '/etc/systemd/system/iftop.service:After=network.target' \
    --append-line '/etc/systemd/system/iftop.service:Requires=network.target' \
    --append-line '/etc/systemd/system/iftop.service:Conflicts=getty@tty1.service' \
    --append-line '/etc/systemd/system/iftop.service:[Service]' \
    --append-line '/etc/systemd/system/iftop.service:ExecStart=/usr/sbin/iftop' \
    --append-line '/etc/systemd/system/iftop.service:StandardInput=tty-force' \
    --append-line '/etc/systemd/system/iftop.service:StandardOutput=inherit' \
    --append-line '/etc/systemd/system/iftop.service:StandardError=inherit' \
    --append-line '/etc/systemd/system/iftop.service:TTYPath=/dev/tty1' \
    --append-line '/etc/systemd/system/iftop.service:[Install]' \
    --append-line '/etc/systemd/system/iftop.service:WantedBy=multi-user.target' \
    --run-command "systemctl enable iftop" \
    --run-command "systemctl disable firewalld" \
    --run-command "systemctl mask --now firewalld || exit 0" \
    --selinux-relabel \
    -o vnf-traffic-view.img

openstack image create --file vnf-traffic-view.img vnf-traffic-view
