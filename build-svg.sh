#!/bin/bash

set -xe

BASENAME=`echo $1 | sed 's/\..*$//'`

# workaround for https://bugs.launchpad.net/ubuntu/+source/inkscape/+bug/1417470
inkscape $1 -j -C --export-latex --export-ps=${BASENAME}.ps
ps2pdf ${BASENAME}.ps ${BASENAME}.pdf
sed "s/[.]ps/.pdf/g" ${BASENAME}.ps_tex > ${BASENAME}.pdf_tex
rm ${BASENAME}.ps_tex ${BASENAME}.ps
