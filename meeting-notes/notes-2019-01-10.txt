- SFC installed on OpenStack instance
  - testing hits errors
    - Column 'host_id' cannot be null
    - host_id maps ports to physical hosts
      - seems set when polling with API?
      https://bugs.launchpad.net/networking-sfc/+bug/1782648
      - error does not occur if OSM creates instances
        - ports are created
  - source VNF is relevant!
    - neutron_source_port corresponds to the vnfd-id-ref, vnfd-connection-point-ref tuple
    - example:
      - traffic generator VNF
        - displays current traffic
        - runs: nc 10.0.0.3 4000
        - ip: 10.0.0.1
      - relay VNF
        - displays current traffic
        - ip: 10.0.0.2
      - target VNF
        - displays current traffic
        - runs: cat /dev/random | nc -kl -p 4000
        - ip: 10.0.0.3
      - classifier
        - source vnf: traffic generator
        - destination: 10.0.0.3[:4000]
    - example does not work? No network activity captured in the relay VNF
      - no port pairs are created in openstack sfc... this was the case for the OSM example?
        - 2019-01-10 03:53:01.614 4927 INFO neutron.api.v2.resource [req-ae66188d-05ee-46e6-984c-7d3c7ad26b86 3b5edec4ba444e14bb02baa837747f1f b18b93b3c5014db08af0c7cfb6927998 - default default] create failed (client error): The server could not comply with the request since it is either malformed or otherwise incorrect.
        - 2019-01-10 03:53:01.615 4927 INFO neutron.wsgi [req-ae66188d-05ee-46e6-984c-7d3c7ad26b86 3b5edec4ba444e14bb02baa837747f1f b18b93b3c5014db08af0c7cfb6927998 - default default] 131.234.28.86 "POST /v2.0/sfc/port_pairs HTTP/1.1" status: 400  len: 374 time: 0.2268970
        - 2019-01-10 09:00:54,956 CRITICAL  openmano.vim.openstack 139868643698432 vim_thread.py:790 Unexpected exception at run: list index out of range
        Traceback (most recent call last):
          File "/root/RO/build/osm_ro/vim_thread.py", line 784, in run
            nb_processed = self._proccess_pending_tasks()
          File "/root/RO/build/osm_ro/vim_thread.py", line 603, in _proccess_pending_tasks
            result, database_update = self.new_classification(task)
          File "/root/RO/build/osm_ro/vim_thread.py", line 1189, in new_classification
            "logical_source_port": interfaces[0],
        IndexError: list index out of range
        - BadRequest: Port Pair ingress port 54edb087-0291-4fc6-9e8a-181e1c9bcb58 does not belong to a host.




    - disk full
      - rm -rf /var/lib/nova/instances/_base
- intro
- use cases
- example VM for VNFFG



open issues -> challenges?


to next meeting:
- get VNFFG example running or backup plan
- intro + background finish, submit for review
  - eta: Mon 21
- testing of full floating use case
- finish description of floating ip implementation
