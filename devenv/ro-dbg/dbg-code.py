#! /usr/bin/python
import sys

print('Preparing debugger!')

sys.path.append('/pycharm-debug.egg')
import pydevd
print('Enabling debugger!')
pydevd.settrace('10.0.2.2', port=21000, stdoutToServer=True, stderrToServer=True, suspend=False)

print('debugger enabled!')
