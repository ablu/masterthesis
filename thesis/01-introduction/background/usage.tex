\subsection{\ac{OSM} Usage}
\label{sec:usage-osm}

The two main interaction types with \ac{OSM} are the command-line client as well as the web-based user interface.

For simplicity, usage instructions and examples within this work are only given for the command-line.
However, the web-based interface offers the same capabilities and should be straight-forward to use with the knowledge of the command-line interface.

The introduction only covers information which is required for the understanding of this work, further details can be acquired from the project's documentation~\cite{osm-wiki}.

\paragraph{Preparation of environment}
The installation of \ac{OSM} is performed by a script~\cite{osm-install-doc} which installs the required software into a clean Ubuntu~16.04 system and launches \ac{OSM}:

\begin{lstlisting}
wget https://osm-download.etsi.org/<@\\@>ftp/osm-5.0-five/install_osm.sh
chmod +x install_osm.sh
./install_osm.sh
\end{lstlisting}

After installation, \ac{OSM} requires a \ac{VIM} for the allocation of resources.
While it supports multiple\footnote{\url{https://osm.etsi.org/wikipub/index.php/OSM_Release_FIVE\#Adding_VIM_accounts}} providers, for this work only OpenStack is relevant.


Therefore a standard, single-node RDO OpenStack Queens\footnote{\url{https://www.rdoproject.org/}} installation was used.
The following commands assume an existing OpenStack installation.

Registering an OpenStack instance mainly requires passing of the OpenStack credentials to \ac{OSM}:

\begin{lstlisting}[numbers=none]
osm vim-create --name <vim-name> --user <username> --password <password> --auth_url <keystone-endpoint>/v3 --tenant <tenant-name> --account_type openstack
\end{lstlisting}

\paragraph{Registration of \ac{VNFD}}
\acp{VNF} in \ac{OSM} are defined by a \ac{VM} image name and the resources which should be made available to the \ac{VNF} instance.
These resources can be diverse, starting with available CPU and RAM allocation reaching to the available number and type of network adapters.
For easier recognition and display within the \ac{VNF} registry developers can also define descriptions and logos.
The resulting description of the \ac{VNF} is called \acp{VNFD} in the \ac{OSM} context.

A minimal \ac{VNFD} is given in \Cref{lst:osm-vnfd-example}.
The described \ac{VNF} features a cirros\footnote{\url{https://launchpad.net/cirros}} \ac{VM} image with a single virtual CPU core, 512 MB of RAM and 10 GB of disk storage.
For network connectivity the \ac{VNF} has a single interface called ``cp0''.
The full reference for the \ac{VNFD} file format can be found at~\cite{osm-vnfd-file}.

\begin{lstlisting}[caption={A minimal example for a \ac{VNFD}.}, label=lst:osm-vnfd-example,float]
vnfd-catalog:
vnfd:
- id: simple-vnf
  name: simple-vnf
  short-name: simple-vnf
  connection-point:
  - name: simple-vnf/cp0
    type: VPORT
  mgmt-interface:
    cp: simple-vnf/cp0
  vdu:
  - id: cirros
    image: cirros
    vm-flavor:
      vcpu-count: 1
      memory-mb: 512
      storage-gb: 10
    interface:
    - name: simple-vnf0
      position: 0
      type: EXTERNAL
      virtual-interface:
        type: VIRTIO
      external-connection-point-ref: simple-vnf/cp0
\end{lstlisting}

\begin{samepage}
In order to register the \ac{VNFD} in \ac{OSM}'s registry, the configuration file needs to be packaged.
For this the descriptor needs to be saved into an otherwise empty folder.
This folder then needs to be archived as \texttt{.tar.gz} file:

\begin{lstlisting}[numbers=none]
tar czf <archive-name>.tar.gz <folder-name>
\end{lstlisting}
\end{samepage}

The resulting archive can then be uploaded using the \texttt{osm} command-line tool:

\begin{lstlisting}[numbers=none]
osm vnfd-create <archive-name>.tar.gz
\end{lstlisting}

The uploaded \ac{VNFD} is now ready for use by an \ac{NS}.


\paragraph{Registration of \ac{NSD}}
Similar to a \ac{VNFD}, the \ac{NSD} is described using a configuration file.
The main content of an \ac{NSD} is a description of a network which aggregates multiple \acp{VNF}. \Cref{lst:osm-nsd-example} shows a simple \ac{NS} which connects two \acp{VNF} which were introduced before.
The full reference, as given in~\cite{osm-nsd-file}, also allows advanced features, such as monitoring, placement metadata or defining service paths.

\begin{lstlisting}[caption={A minimal example for an \ac{NSD}.}, label=lst:osm-nsd-example,float]
nsd-catalog:
nsd:
- id: simple-ns
  name: simple-ns
  short-name: simple-ns
  constituent-vnfd:
  - member-vnf-index: '1'
    vnfd-id-ref: simple-vnf
  - member-vnf-index: '2'
    vnfd-id-ref: simple-vnf
  vld:
  - id: mgmt_vl
    type: ELAN
    mgmt-network: 'true'
    vnfd-connection-point-ref:
    - member-vnf-index-ref: '1'
      vnfd-connection-point-ref: simple-vnf/cp0
      vnfd-id-ref: simple-vnf
    - member-vnf-index-ref: '2'
      vnfd-connection-point-ref: simple-vnf/cp0
      vnfd-id-ref: simple-vnf
\end{lstlisting}

\begin{samepage}
The \ac{NSD} file can then be packaged and uploaded in a similar way as the \acp{VNFD}:
\begin{lstlisting}[numbers=none]
tar czf <archive-name>.tar.gz <nsd-folder-name>
osm nsd-create <archive-name>.tar.gz
\end{lstlisting}
\end{samepage}

\paragraph{Instantiation of \acp{VNF}}
After the onboarding process is complete, an \ac{NS} can be instantiated by creating it from an \ac{NSD}.
This is done by selecting the \ac{NSD}, the target \ac{VIM} and a name for the service:

\begin{lstlisting}[numbers=none]
osm ns-create --nsd_name <name-of-nsd> --ns_name <instance-name> --vim_account <vim-name>
\end{lstlisting}

Ultimately, the \ac{RO} component of \ac{OSM} handles this request and triggers the described deployment with the information from the \ac{NSD} and referenced \acp{VNFD}.
