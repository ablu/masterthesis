\section{Support for Juju~Charm based configuration of \acp{VNF}}
\label{sec:charms-impl}
As motivated in \Cref{sec:motivation-charms}, Juju~Charms are an important tool for the configuration of \acp{VNF}.
For example, a standard firewall \ac{VNF} is only of limited use if no custom rules could be installed for describing the traffic which should be blocked.
Such custom configurations are usually performed by executing commands within the machine or otherwise sending a request to the \ac{VNF} to configure.

These kind of use cases are currently not usable with \texttt{vim-emu} since the execution of the Charms happens in \ac{LXC} containers which run outside of the emulated topology.
This results in the \ac{LXC} containers being unable to communicate to the IP addresses of \texttt{vim-emu} instances which would only be resolved within the emulated environment.
This disallows the usage of \texttt{vim-emu} for prototyping \acp{VNF} containing Charms, which requires repetitive deployments on slow, fully-featured \acp{VIM}, turning a quick test of a new Charm iteration into a cumbersome process.
Therefore, a quicker deployment using \texttt{vim-emu} could greatly speed up the feedback loop during development, enhancing the developer experience in this scenario.

Thus, in the following sections an example use case will be defined in which a \ac{VNF} will be configured using \ac{SSH}.
A solution for this use case is developed, demonstrating the ability to connect to the \ac{VNF} and issue arbitrary commands from a Juju~Charm.


\subsection{Use Case Example}
A very simple configuration action could be the creation of a specific file within a \ac{VNF}.
Under Linux this can be done using the command \texttt{touch <file>} which will create a new file if the specified filename does not exist or otherwise update the modification timestamp to the current time.
In fact, \ac{OSM} already provides an example Charm \texttt{simple}~\cite{osm-example-charm-simple}, which will be used throughout the remainder of this section.
For reference the relevant code of the Charm is given in \Cref{lst:osm-example-charm-simple}.
While the invocation of \texttt{touch} via \ac{SSH} is only a very simple example, proving that \ac{SSH} works opens the possibility of arbitrary command execution or other types of connections to the \ac{VNF}.

\begin{lstlisting}[caption={Example Juju~Charm code from the devops repository~\cite{osm-example-charm-simple}. The example uses \ac{SSH} in order to invoke \texttt{touch} with the \texttt{filename} parameter as argument whenever the \texttt{touch} action is received.}, label=lst:osm-example-charm-simple,language=python,float]
@when('sshproxy.configured')
@when_not('simple.installed')
def install_simple_proxy_charm():
    set_flag('simple.installed')
    status_set('active', 'Ready!')

@when('actions.touch')
def touch():
    err = ''
    try:
        filename = action_get('filename')
        cmd = ['touch {}'.format(filename)]
        result, err = charms.sshproxy._run(cmd)
    except:
        action_fail('command failed:' + err)
    else:
        action_set({'output': result})
    finally:
        clear_flag('actions.touch')
\end{lstlisting}

Since the touched filename is configurable, the Charm can also be used as an example for ``day-1'' configuration by triggering a touch using different filenames during the runtime of a \ac{VNF}.

As demonstration \ac{VNFD}, \Cref{lst:osm-vnfd-example} was extended as shown in \Cref{lst:vnfd-charmed-use-case}.
The \ac{VNFD} will first configure the Charm with the management IP of the \ac{VNF} instance and afterwards triggers the \texttt{touch} with the parameter \texttt{filename=/test}.
The described example matches the illustration given in \Cref{fig:charm-lifecycle}.

\Cref{lst:vnfd-charmed-use-case} references the image \texttt{charmed}, which therefore has to provide a way to handle \ac{SSH} connections.
For this the Dockerfile from \Cref{lst:charmed-docker} is built and assumed to be available in the Docker registry under that name.
For reference, the build process of a matching \ac{VM} image is given in \Cref{lst:charmed-vm-build}.

\begin{lstlisting}[caption={Dockerfile which builds an image allowing \ac{SSH} login with user \texttt{root} and password \texttt{test}. Based on~\cite{docker-ssh}, adapted to make compatible with \texttt{vim-emu} requirements.}, label=lst:charmed-docker,float]
FROM ubuntu:16.04

RUN apt update && apt install -y net-tools iproute
RUN apt install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:test' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
ENV SON_EMU_CMD "/usr/sbin/sshd"
\end{lstlisting}

\begin{lstlisting}[caption={The \ac{VNFD} used for testing \texttt{vim-emu}'s support of Juju~Charms. For brevity only the required extension to the minimal \ac{VNFD} example (\Cref{lst:osm-vnfd-example}) is shown. The descriptor will trigger a deployment of the Charm \texttt{simple} and configure the Charm to use the management IP of the \ac{VNF} as \ac{SSH} connection target. The connection will use the user \texttt{root} with password \texttt{test}. After configuration, the \texttt{touch} primitive will be executed with the parameter \texttt{filename='/test'}. Finally, the primitive \texttt{touch} is also made available as manually executable primitive where the default value of \texttt{filename} is set to \texttt{'/testmanual'}.}, label=lst:vnfd-charmed-use-case,float]
vnfd:
  ...
  vdu:
    ...
    image: charmed
    ...
  vnf-configuration:
    juju:
      charm: simple
    initial-config-primitive:
    - seq: '1'
      name: config
      parameter:
      - name: ssh-hostname
        value: <rw_mgmt_ip>
      - name: ssh-username
        value: root
      - name: ssh-password
        value: test
    - seq: '2'
      name: touch
      parameter:
      - name: filename
        value: /test
    config-primitive:
    - name: touch
      parameter:
      - name: filename
        data-type: STRING
        default-value: '/testmanual'
\end{lstlisting}

\subsection{Current Status}
When deploying the example \ac{VNFD} using \texttt{vim-emu}, no creation of the file \texttt{/test} within the deployed \ac{VNF} instance can be observed.
Additionally, manually issuing a ``day-1'' configuration using \texttt{osm ns-action test1 --action\_name touch --params '{}' --vnf\_name 1} does not lead to the creation of \texttt{/testmanual}.
Here, both commands fail with an error message indicating a failing connection to the \ac{VNF}.
However, when executing the same example on OpenStack the connection and configuration works.

The reason for this is that a floating IP mechanism is used by OpenStack for assigning a public IP to otherwise private instances.
\texttt{vim-emu} tries to fake this mechanism, but defines a hard-coded IP range for floating IPs.
The relevant code is shown in \Cref{lst:vim-emu-floating-ip-impl} and reveals that no actual tracking or mapping of assigned IPs is performed.

Now, \ac{OSM}'s \ac{RO} module repetitively polls (\Cref{lst:osm-ro-floating-ip-get}) the status of a launched \ac{VM} in order to determine the assigned IP address.
It achieves this by calling the floating IP \ac{API} while filtering by the port which was assigned to the \ac{VNF} instance.

\begin{lstlisting}[caption={(Simplified) Floating~IP implementation of \texttt{vim-emu}~\cite{vim-emu-floating-ip-impl}. The IPs 172.0.0.100-172.0.0.109 are mapped to 10.0.0.100-10.0.0.109.}, label=lst:vim-emu-floating-ip-impl,language=Python,float]
for i in range(100, 110):
    ip = dict()
    # ...
    ip["port_id"] = "port_id"
    ip["floating_ip_address"] = "172.0.0.%d" % i
    ip["fixed_ip_address"] = "10.0.0.%d" % i
    resp["floatingips"].append(ip)
return Response(json.dumps(resp), status=200,
                mimetype='application/json')
\end{lstlisting}

\begin{lstlisting}[caption={Implementation of the IP determination logic of the \ac{RO} module~\cite{ro-floating-ip-get}.}, label=lst:osm-ro-floating-ip-get,language=Python,float,firstnumber=1455]
floating_ip_dict = self.neutron.list_floatingips(port_id=port["id"])
if floating_ip_dict.get("floatingips"):
    ips.append(floating_ip_dict["floatingips"][0] .get("floating_ip_address") )
\end{lstlisting}

Since the \texttt{vim-emu} implementation does not take into account the \texttt{port\_id} filter, but always returns the fixed range of 172.0.0.100-110, \ac{OSM} is made to believe that any newly created port (and thus \ac{VNF}) has the IP address 172.0.0.100.
While this leads to issues with multiple \ac{VNF} instances, in fact, the floating IP range has no relation to the IP range assigned to the emulated network, resulting in even the first container being advertised under a wrong IP address.

\subsection{Solution Idea}
In order to solve the problem described above, a sensible, routeable IP address has to be returned for each port of an emulated instance.
A simple solution would be to reuse the existing IP address that Docker assigns to each container.
Under Linux this IP address can be used in order to communicate to the Docker container from the host system essentially being an equivalent to OpenStack's floating IPs.
However, \texttt{vim-emu} allows topologies with multiple ports per container and internally creates additional interfaces which are attached to the Docker container.
The original interface which was provided by Docker is left in place, but the software within the container is configured to send all traffic along the newly created interfaces using custom IP routes.

This means that, unlike floating IPs, the IP address provided by Docker will not match to any port that is part of the topology.
This could potentially restrict use cases where an emulated \ac{VNF} expects a management connection to occur on a specific port.

An alternative solution, which also covers that specific use case, would involve to manually create routing rules in order to allow access to the emulated datacenter routers from the host system of \texttt{vim-emu} and configuring the routers to forward the traffic to the respective target port.
This could be either done by using a separate subnet of public IP addresses or by reusing the private IP addresses assigned to the individual ports.
Using a separate subnet of public IP addresses would be the closest imitation of OpenStack, but would further increase the complexity due to the required translation from public to private IP addresses.

Additionally, the general concept of allowing access from the host system to the emulated nodes is against Mininet's concept of providing an isolated environment.
Thus, connections from the host are not expressed by the emulated topology and Mininet features such as emulating link quality using Linux \ac{TC} would require custom solutions.

All in all, the analysis reveals that providing an interoperability at the same level as OpenStack, while supporting Mininet's emulation features, is a non-trivial task.
However, a simple solution which reuses the existing Docker public IP address, is easily implemented, yet still covers a wide range of use cases including the initial demand of being able to test and prototype Juju~Charms.
While testing Juju~Charms, the actual port which receives a connection usually does not matter and link quality controls are not that relevant for the Charm development itself.
Furthermore, most of the described issues are the result of attempting to mix existing infrastructure with the emulated infrastructure.
If all relevant components could be executed within the emulated infrastructure (an approach which is evaluated in \Cref{sec:full-stack-emulation}), the described issues do not occur.
Due to that, this work will only implement the simple solution.

\subsection{Implementation and Test}
While determining the defined IP address of the Docker container is technically a simple \ac{API} call to the Docker daemon, a small additional challenge is to map to the Docker instance from the emulated port.
However, this is easily solved by tracking the assigned instance on the port side.
With this mapping being tracked, the floating IP \ac{API} merely has to detect the filter on the port, fetch the specified port and return the IP address of the assigned instance.
This is implemented in Changeset~7022\footnote{\url{https://osm.etsi.org/gerrit/7022/}}.
In fact, calling the Docker daemon in order to retrieve the assigned IP was not necessary since \texttt{vim-emu} already tracked the \ac{API} result of the instance creation command within the instance's \texttt{dcinfo} field.
Calling \texttt{dcinfo["NetworkSettings"]["IPAddress"]} returns the desired IP address, which then can be returned by the faked floating IP \ac{API}.

After deploying the previously defined \ac{VNFD}, the code from \Cref{lst:charm-test-example} can be used to confirm a working Charm interaction.
Here, first the existence of \texttt{/test} and the non-existence of \texttt{/testmanual} is confirmed. The file \texttt{/test} was expected to be created during the ``day-0'' configuration and the ``day-1'' configuration which triggers the creation of \texttt{/testmanual} did not occur yet.
Then, the ``day-1'' configuration is triggered by executing \texttt{osm ns-action}, after which the file \texttt{/testmanual} becomes available.

\begin{lstlisting}[caption={Test for demonstrating that the Charm configuration worked. \texttt{ls} prints an error message if the file which is attempted to be listed does not exist and returns the filename otherwise.}, label=lst:charm-test-example,float]
# docker exec mn.dc1_test1-1--1 ls /test
/test
# docker exec mn.dc1_test1-1--1 ls /testmanual
ls: cannot access '/testmanual': No such file or directory
# osm ns-action test1 --action_name touch --params '{}' --vnf_name 1
# docker exec mn.dc1_test1-1--1 ls /testmanual
/testmanual
\end{lstlisting}