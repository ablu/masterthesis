PROPOSAL_DOCUMENT_NAME=Proposal\ Master\ Thesis\ Erik\ Schilling
PRESENTATION_DOCUMENT_NAME=Presentation\ Proposal\ Master\ Thesis\ Erik\ Schilling
PRESENTATION_HACKFEST_DOCUMENT_NAME=Presentation\ Proposal\ Hackfest
PRESENTATION_DEFENSE_DOCUMENT_NAME=Presentation\ Defense\ Master\ Thesis\ Erik\ Schilling
THESIS_DOCUMENT_NAME=Master\ Thesis\ Erik\ Schilling
THESIS_FIGURES_SVG=$(wildcard thesis/figures/*.svg)
THESIS_FIGURES_DIA=$(wildcard thesis/figures/*.dia)

all: build/${PROPOSAL_DOCUMENT_NAME}.pdf build/${PRESENTATION_DOCUMENT_NAME}.pdf build/${THESIS_DOCUMENT_NAME}.pdf build/${PRESENTATION_HACKFEST_DOCUMENT_NAME}.pdf build/${PRESENTATION_DEFENSE_DOCUMENT_NAME}.pdf

build/${PROPOSAL_DOCUMENT_NAME}.pdf: $(shell find proposal/ -iname '*.tex' -or -iname '*.bib' | sed 's/ /\\ /g')
	mkdir -p build && \
	cd proposal && \
	latexmk -output-directory=aux/ -interaction=nonstopmode -pdf ${PROPOSAL_DOCUMENT_NAME}.tex && \
	cp aux/${PROPOSAL_DOCUMENT_NAME}.pdf ../build

build/${THESIS_DOCUMENT_NAME}.pdf: thesis_figures plots $(shell find thesis/ -iname '*.tex' -or -iname '*.bib' -or -iname '*.json' | sed 's/ /\\ /g'; find thesis/listings/)
	mkdir -p build && \
	cd thesis && \
	latexmk -r ../peng.rc -output-directory=aux/ -interaction=nonstopmode -pdf ${THESIS_DOCUMENT_NAME}.tex && \
	cp aux/${THESIS_DOCUMENT_NAME}.pdf ../build


build/${PRESENTATION_DOCUMENT_NAME}.pdf: $(shell find presentation_proposal/ -iname '*')
	mkdir -p build && \
	cd presentation_proposal && \
	./build.sh

build/${PRESENTATION_HACKFEST_DOCUMENT_NAME}.pdf: $(shell find presentation_proposal_hackfest/ -iname '*')
	mkdir -p build && \
	cd presentation_proposal_hackfest && \
	./build.sh

build/${PRESENTATION_DEFENSE_DOCUMENT_NAME}.pdf: $(shell find presentation_defense/ -iname '*')
	mkdir -p build && \
	cd presentation_defense && \
	./build.sh

thesis_figures: $(THESIS_FIGURES_SVG:.svg=.pdf_tex) $(THESIS_FIGURES_DIA:.dia=.pdf)

%.pdf_tex: %.svg build-svg.sh
	./build-svg.sh $< $@

%.pdf: %.dia build-dia.sh
	./build-dia.sh $< $@

plots: $(shell find experiment-data/ -iname '*')
	cd experiment-data && ./generate-plots.sh

clean:
	rm -rf build/
