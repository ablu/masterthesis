[root@fgcn-of-25 ~(keystone_admin)]# ovs-ofctl dump-flows br-int
 cookie=0xc8c1bdcd9bc81a9d, duration=2075075.819s, table=0, n_packets=0, n_bytes=0, priority=65535,vlan_tci=0x0fff/0x1fff actions=drop
 cookie=0xb1faa0d38cd1fd9c, duration=29.297s, table=0, n_packets=0, n_bytes=0, priority=30,tcp,in_port="qvo29ef6007-52",nw_src=10.0.0.10,nw_dst=10.0.0.12,tp_dst=80 actions=group:1
 cookie=0xb1faa0d38cd1fd9c, duration=28.196s, table=0, n_packets=0, n_bytes=0, priority=30,tcp,in_port="qvoc2ddbffb-fd",nw_src=10.0.0.10,nw_dst=10.0.0.12,tp_dst=80 actions=group:2
 cookie=0xb1faa0d38cd1fd9c, duration=24.930s, table=0, n_packets=0, n_bytes=0, priority=30,tcp,in_port="qvo383859f7-6a",nw_src=10.0.0.10,nw_dst=10.0.0.12,tp_dst=80 actions=NORMAL
 cookie=0xb1faa0d38cd1fd9c, duration=2075075.262s, table=0, n_packets=0, n_bytes=0, priority=20,mpls actions=resubmit(,10)
 cookie=0xb1faa0d38cd1fd9c, duration=2075075.261s, table=0, n_packets=0, n_bytes=0, priority=20,dl_type=0x894f actions=resubmit(,10)
 cookie=0xc8c1bdcd9bc81a9d, duration=33.419s, table=0, n_packets=0, n_bytes=0, priority=10,icmp6,in_port="qvo29ef6007-52",icmp_type=136 actions=resubmit(,24)
 cookie=0xc8c1bdcd9bc81a9d, duration=27.765s, table=0, n_packets=0, n_bytes=0, priority=10,icmp6,in_port="qvoc2ddbffb-fd",icmp_type=136 actions=resubmit(,24)
 cookie=0xc8c1bdcd9bc81a9d, duration=24.803s, table=0, n_packets=0, n_bytes=0, priority=10,icmp6,in_port="qvo383859f7-6a",icmp_type=136 actions=resubmit(,24)
 cookie=0xc8c1bdcd9bc81a9d, duration=33.415s, table=0, n_packets=10, n_bytes=420, priority=10,arp,in_port="qvo29ef6007-52" actions=resubmit(,24)
 cookie=0xc8c1bdcd9bc81a9d, duration=27.762s, table=0, n_packets=9, n_bytes=378, priority=10,arp,in_port="qvoc2ddbffb-fd" actions=resubmit(,24)
 cookie=0xc8c1bdcd9bc81a9d, duration=24.800s, table=0, n_packets=7, n_bytes=294, priority=10,arp,in_port="qvo383859f7-6a" actions=resubmit(,24)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075075.565s, table=0, n_packets=2, n_bytes=140, priority=2,in_port="int-br-ex" actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=33.424s, table=0, n_packets=16, n_bytes=2068, priority=9,in_port="qvo29ef6007-52" actions=resubmit(,25)
 cookie=0xc8c1bdcd9bc81a9d, duration=27.770s, table=0, n_packets=18, n_bytes=2244, priority=9,in_port="qvoc2ddbffb-fd" actions=resubmit(,25)
 cookie=0xc8c1bdcd9bc81a9d, duration=24.808s, table=0, n_packets=14, n_bytes=1936, priority=9,in_port="qvo383859f7-6a" actions=resubmit(,25)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075073.018s, table=0, n_packets=5307, n_bytes=9058467, priority=3,in_port="int-br-ex",vlan_tci=0x0000/0x1fff actions=mod_vlan_vid:2,resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075075.823s, table=0, n_packets=92478, n_bytes=17443974, priority=0 actions=resubmit(,60)
 cookie=0xb1faa0d38cd1fd9c, duration=29.386s, table=5, n_packets=0, n_bytes=0, priority=0,ip,dl_dst=fa:16:3e:68:e4:a7 actions=push_mpls:0x8847,load:0x1ff->OXM_OF_MPLS_LABEL[],set_mpls_ttl(255),mod_vlan_vid:16,resubmit(,10)
 cookie=0xb1faa0d38cd1fd9c, duration=28.289s, table=5, n_packets=0, n_bytes=0, priority=0,ip,dl_dst=fa:16:3e:32:b7:53 actions=push_mpls:0x8847,load:0x1fe->OXM_OF_MPLS_LABEL[],set_mpls_ttl(254),mod_vlan_vid:16,resubmit(,10)
 cookie=0xb1faa0d38cd1fd9c, duration=28.169s, table=10, n_packets=0, n_bytes=0, priority=1,mpls,dl_vlan=16,dl_dst=fa:16:3e:68:e4:a7,mpls_label=511 actions=strip_vlan,pop_mpls:0x0800,output:"qvoc2ddbffb-fd"
 cookie=0xb1faa0d38cd1fd9c, duration=24.881s, table=10, n_packets=0, n_bytes=0, priority=1,mpls,dl_vlan=16,dl_dst=fa:16:3e:32:b7:53,mpls_label=510 actions=strip_vlan,pop_mpls:0x0800,output:"qvo383859f7-6a"
 cookie=0xb1faa0d38cd1fd9c, duration=2075075.260s, table=10, n_packets=8, n_bytes=592, priority=0 actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=2075075.824s, table=23, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=33.422s, table=24, n_packets=0, n_bytes=0, priority=2,icmp6,in_port="qvo29ef6007-52",icmp_type=136,nd_target=fe80::f816:3eff:fed3:d9dd actions=resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=27.766s, table=24, n_packets=0, n_bytes=0, priority=2,icmp6,in_port="qvoc2ddbffb-fd",icmp_type=136,nd_target=fe80::f816:3eff:fe68:e4a7 actions=resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=24.805s, table=24, n_packets=0, n_bytes=0, priority=2,icmp6,in_port="qvo383859f7-6a",icmp_type=136,nd_target=fe80::f816:3eff:fe32:b753 actions=resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=33.417s, table=24, n_packets=10, n_bytes=420, priority=2,arp,in_port="qvo29ef6007-52",arp_spa=10.0.0.10 actions=resubmit(,25)
 cookie=0xc8c1bdcd9bc81a9d, duration=27.763s, table=24, n_packets=9, n_bytes=378, priority=2,arp,in_port="qvoc2ddbffb-fd",arp_spa=10.0.0.11 actions=resubmit(,25)
 cookie=0xc8c1bdcd9bc81a9d, duration=24.802s, table=24, n_packets=7, n_bytes=294, priority=2,arp,in_port="qvo383859f7-6a",arp_spa=10.0.0.12 actions=resubmit(,25)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075075.820s, table=24, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=33.428s, table=25, n_packets=24, n_bytes=2348, priority=2,in_port="qvo29ef6007-52",dl_src=fa:16:3e:d3:d9:dd actions=resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=27.774s, table=25, n_packets=25, n_bytes=2482, priority=2,in_port="qvoc2ddbffb-fd",dl_src=fa:16:3e:68:e4:a7 actions=resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=24.817s, table=25, n_packets=20, n_bytes=2160, priority=2,in_port="qvo383859f7-6a",dl_src=fa:16:3e:32:b7:53 actions=resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075075.822s, table=60, n_packets=215714, n_bytes=37554640, priority=3 actions=NORMAL
[root@fgcn-of-25 ~(keystone_admin)]# 



[root@fgcn-of-25 ~(keystone_admin)]# ovs-ofctl dump-groups br-int
NXST_GROUP_DESC reply (xid=0x2):
 group_id=1,type=select,bucket=bucket_id:0,actions=mod_dl_dst:fa:16:3e:68:e4:a7,resubmit(,5)
 group_id=2,type=select,bucket=bucket_id:0,actions=mod_dl_dst:fa:16:3e:32:b7:53,resubmit(,5)
[root@fgcn-of-25 ~(keystone_admin)]# 




[root@fgcn-of-25 ~(keystone_admin)]# ovs-vsctl show
c0b7259c-cf8d-40d1-95a5-c3250ecf14d0
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge br-ex
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port br-ex
            Interface br-ex
                type: internal
        Port phy-br-ex
            Interface phy-br-ex
                type: patch
                options: {peer=int-br-ex}
    Bridge br-tun
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port patch-int
            Interface patch-int
                type: patch
                options: {peer=patch-tun}
        Port br-tun
            Interface br-tun
                type: internal
    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port "qvo383859f7-6a"
            tag: 16
            Interface "qvo383859f7-6a"
        Port "qg-43447d7a-d0"
            tag: 2
            Interface "qg-43447d7a-d0"
                type: internal
        Port "qvo29ef6007-52"
            tag: 16
            Interface "qvo29ef6007-52"
        Port "tap0d1cb496-c6"
            tag: 13
            Interface "tap0d1cb496-c6"
                type: internal
        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}
        Port "qvoc2ddbffb-fd"
            tag: 16
            Interface "qvoc2ddbffb-fd"
        Port "qr-199cd766-ce"
            tag: 13
            Interface "qr-199cd766-ce"
                type: internal
        Port int-br-ex
            Interface int-br-ex
                type: patch
                options: {peer=phy-br-ex}
        Port br-int
            Interface br-int
                type: internal
        Port "tap164ad170-9e"
            tag: 16
            Interface "tap164ad170-9e"
                type: internal
    ovs_version: "2.10.1"
[root@fgcn-of-25 ~(keystone_admin)]# 

[root@fgcn-of-25 ~(keystone_admin)]# ovs-ofctl dump-ports br-int
OFPST_PORT reply (xid=0x2): 10 ports
  port "qvoc2ddbffb-fd": rx pkts=190, bytes=9732, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=375, bytes=19794, drop=0, errs=0, coll=0
  port LOCAL: rx pkts=0, bytes=0, drop=34703, errs=0, frame=0, over=0, crc=0
           tx pkts=0, bytes=0, drop=0, errs=0, coll=0
  port "qvo29ef6007-52": rx pkts=191, bytes=9682, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=373, bytes=19754, drop=0, errs=0, coll=0
  port "tap0d1cb496-c6": rx pkts=337, bytes=17276, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=21, bytes=4380, drop=0, errs=0, coll=0
  port "qvo383859f7-6a": rx pkts=188, bytes=9556, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=376, bytes=19880, drop=0, errs=0, coll=0
  port "qr-199cd766-ce": rx pkts=84286, bytes=6799465, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=86750, bytes=16848877, drop=0, errs=0, coll=0
  port  "int-br-ex": rx pkts=5309, bytes=9058607, drop=?, errs=?, frame=?, over=?, crc=?
           tx pkts=37601, bytes=3319716, drop=?, errs=?, coll=?
  port "tap164ad170-9e": rx pkts=546, bytes=19448, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=14, bytes=3812, drop=0, errs=0, coll=0
  port  "patch-tun": rx pkts=0, bytes=0, drop=?, errs=?, frame=?, over=?, crc=?
           tx pkts=34705, bytes=3021734, drop=?, errs=?, coll=?
  port "qg-43447d7a-d0": rx pkts=15384, bytes=10634505, drop=0, errs=0, frame=0, over=0, crc=0
           tx pkts=3143, bytes=208826, drop=0, errs=0, coll=0



[root@fgcn-of-25 ~(keystone_admin)]# ovs-ofctl dump-ports-desc br-int
OFPST_PORT_DESC reply (xid=0x2):
 1(int-br-ex): addr:6e:89:c8:72:5e:2b
     config:     0
     state:      0
     speed: 0 Mbps now, 0 Mbps max
 2(patch-tun): addr:be:26:5c:b2:ba:62
     config:     0
     state:      0
     speed: 0 Mbps now, 0 Mbps max
 67(qg-43447d7a-d0): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 70(tap0d1cb496-c6): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 71(qr-199cd766-ce): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 89(tap164ad170-9e): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 90(qvo29ef6007-52): addr:2a:f7:58:04:86:d9
     config:     0
     state:      0
     current:    10GB-FD COPPER
     speed: 10000 Mbps now, 0 Mbps max
 91(qvoc2ddbffb-fd): addr:66:14:a9:e9:c0:a0
     config:     0
     state:      0
     current:    10GB-FD COPPER
     speed: 10000 Mbps now, 0 Mbps max
 92(qvo383859f7-6a): addr:32:82:51:c5:e0:d8
     config:     0
     state:      0
     current:    10GB-FD COPPER
     speed: 10000 Mbps now, 0 Mbps max
 LOCAL(br-int): addr:46:77:00:e5:bf:41
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
[root@fgcn-of-25 ~(keystone_admin)]# 



AFTER DELETE:

[root@fgcn-of-25 ~(keystone_admin)]# ovs-ofctl dump-flows br-int
 cookie=0xc8c1bdcd9bc81a9d, duration=2075962.139s, table=0, n_packets=0, n_bytes=0, priority=65535,vlan_tci=0x0fff/0x1fff actions=drop
 cookie=0xb1faa0d38cd1fd9c, duration=2075961.582s, table=0, n_packets=0, n_bytes=0, priority=20,mpls actions=resubmit(,10)
 cookie=0xb1faa0d38cd1fd9c, duration=2075961.581s, table=0, n_packets=0, n_bytes=0, priority=20,dl_type=0x894f actions=resubmit(,10)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075959.338s, table=0, n_packets=5307, n_bytes=9058467, priority=3,in_port="int-br-ex",vlan_tci=0x0000/0x1fff actions=mod_vlan_vid:2,resubmit(,60)
 cookie=0xc8c1bdcd9bc81a9d, duration=2075961.885s, table=0, n_packets=2, n_bytes=140, priority=2,in_port="int-br-ex" actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=2075962.143s, table=0, n_packets=92478, n_bytes=17443974, priority=0 actions=resubmit(,60)
 cookie=0xb1faa0d38cd1fd9c, duration=2075961.580s, table=10, n_packets=8, n_bytes=592, priority=0 actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=2075962.144s, table=23, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=2075962.140s, table=24, n_packets=0, n_bytes=0, priority=0 actions=drop
 cookie=0xc8c1bdcd9bc81a9d, duration=2075962.142s, table=60, n_packets=216838, n_bytes=37601912, priority=3 actions=NORMAL
[root@fgcn-of-25 ~(keystone_admin)]# 


[root@fgcn-of-25 ~(keystone_admin)]# ovs-ofctl dump-ports-desc br-int
OFPST_PORT_DESC reply (xid=0x2):
 1(int-br-ex): addr:6e:89:c8:72:5e:2b
     config:     0
     state:      0
     speed: 0 Mbps now, 0 Mbps max
 2(patch-tun): addr:be:26:5c:b2:ba:62
     config:     0
     state:      0
     speed: 0 Mbps now, 0 Mbps max
 67(qg-43447d7a-d0): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 70(tap0d1cb496-c6): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 71(qr-199cd766-ce): addr:00:00:00:00:00:00
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
 LOCAL(br-int): addr:46:77:00:e5:bf:41
     config:     PORT_DOWN
     state:      LINK_DOWN
     speed: 0 Mbps now, 0 Mbps max
[root@fgcn-of-25 ~(keystone_admin)]# 


[root@fgcn-of-25 ~(keystone_admin)]# ovs-vsctl show
c0b7259c-cf8d-40d1-95a5-c3250ecf14d0
    Manager "ptcp:6640:127.0.0.1"
        is_connected: true
    Bridge br-ex
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port br-ex
            Interface br-ex
                type: internal
        Port phy-br-ex
            Interface phy-br-ex
                type: patch
                options: {peer=int-br-ex}
    Bridge br-tun
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port patch-int
            Interface patch-int
                type: patch
                options: {peer=patch-tun}
        Port br-tun
            Interface br-tun
                type: internal
    Bridge br-int
        Controller "tcp:127.0.0.1:6633"
            is_connected: true
        fail_mode: secure
        Port "qg-43447d7a-d0"
            tag: 2
            Interface "qg-43447d7a-d0"
                type: internal
        Port "tap0d1cb496-c6"
            tag: 13
            Interface "tap0d1cb496-c6"
                type: internal
        Port patch-tun
            Interface patch-tun
                type: patch
                options: {peer=patch-int}
        Port "qr-199cd766-ce"
            tag: 13
            Interface "qr-199cd766-ce"
                type: internal
        Port int-br-ex
            Interface int-br-ex
                type: patch
                options: {peer=phy-br-ex}
        Port br-int
            Interface br-int
                type: internal
    ovs_version: "2.10.1"
[root@fgcn-of-25 ~(keystone_admin)]# 
