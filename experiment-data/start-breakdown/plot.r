library(tidyverse)
library(gmodels)

plot_width <- 6
plot_height <- 3.5

d = read.csv("start-breakdown/merged.csv") %>%
  rename(Zookeeper=zookeeper, `General Network \n Setup / Other`=other, Kafka=kafka, LCM=lcm, Mongo=mongo, NBI=nbi, RO=ro, MySQL=ro_db) %>%
  gather(Var, Value) %>%
  group_by(Var) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )
  
ggplot(d, aes(x=Var, y=mean, ymin=low, ymax=high)) +
  geom_col(position="dodge", width=0.2, fill="#F8766D") +
  geom_errorbar(position="dodge", width=0.1, size=0.2) +
  scale_x_discrete(name="Component") + 
  scale_y_continuous(name="Mean startup time [s]", breaks=seq(0,25,2)) +
  theme_bw()
ggsave('../thesis/figures/plot_start_breakdown.eps', width=plot_width, height=plot_height)
