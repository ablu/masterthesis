======================================================
 Telco in a Box: Emulating Full-Stack NFV Deployments
======================================================
:Author: `Erik Schilling <mail@ablu.org>`_
:Date: 2019-04-09


Introduction
============

VNFs
----
.. image:: figures/vnfs.svg
    :width: 100%
- Replace special hardware
- Simple functions chained to form Network Services
- More flexibility

------------------------------

.. image:: figures/osm-logo.svg
    :width: 80%
    :align: center

`**O**pen **S**ource **M**anagement **a****n**d **O**rchestration <https://osm.etsi.org/>`_

- Implements ETSI specifications for VNFs
- Collaboration of industry and research


OSM - VNFD
----------
.. class:: columns

    .. class:: column
        :width: 40%

    .. code:: yaml

        vdu:
          vm-flavor:
            vcpu-count: 1
            image: cirros034
          interface:
          - name: eth0
        connection-point:
        - name: eth0

    .. class:: column
        :width: 65%

        **V**irtual **N**etwork **F**unction **D**efinition

        - Greatly simplified here
        - Defines source image and constraints
        - Exports connection points
        - Not deployable on its own

OSM - NSD
---------
.. class:: columns

    .. class:: column
        :width: 50%

    .. code:: yaml

        constituent-vnfd:
        - member-vnf-index: 1
          vnfd-id-ref: cirros_vnfd
        # ...
        vld:
        - member-vnf-index-ref: 1
          vnfd-connection-point-ref: eth0
        # ...

    .. class:: column
        :width: 50%

        **N**etwork **S**ervice **D**efinition

        - Aggregates individual VNFDs to complex services
        - Deployable on its own


`Juju Charms <https://jujucharms.com/>`_
----------------------------------------
.. image:: figures/juju-charms.svg
    :width: 100%

- Abstraction for low-level management tasks
- Exports set of actions and states

Service Function Chaining
-------------------------
.. image:: figures/service-function-chaining.svg
    :width: 100%

Architecture
------------
.. image:: figures/architecture.svg
    :width: 100%

Development Workflow
--------------------

.. image:: figures/development-workflow-generated.svg
    :width: 100%

Challenges
----------

.. class:: columns

    .. class:: column
        :width: 80%

    - Slow feedback cycle
    - Involving setup and maintenance
    - Hard to automate

    .. class:: column
        :width: 20%

    .. image:: figures/exclamation.svg
        :width: 100%

**Limits adoption of NFV**


Related Work
============

OPNFV - **O**pen **P**latform for **NFV**
-----------------------------------------


Projects of the Linux Foundation Networking Fund:

+---------------+-----------------------------------------------+
| Functest      | Tests functionality of VIM                    |
+---------------+-----------------------------------------------+
| QTIP          | Benchmarking framework                        |
+---------------+-----------------------------------------------+
| Bottlenecks   | System limit testing                          |
+---------------+-----------------------------------------------+
| Yardstick     | Compliance testing                            |
+---------------+-----------------------------------------------+
| StorePerf     | Storage performance testing                   |
+---------------+-----------------------------------------------+
| VSperf        | Data-plane performance testing                |
+---------------+-----------------------------------------------+

ONAP OTF
--------


.. class:: columns

    .. class:: column
        :width: 80%

        Testing subproject for the ONAP platform.

        - Largely work-in-progress
        - Focus on verification and certification

    .. class:: column
        :width: 20%

    .. image:: figures/onap-logo.png
        :width: 100%

NIEP
----

**N**FV **I**nfrastructure **E**mulation **P**latform

- Allows emulation of VNF using Click-on-OSv
- Minimized VM images which are combined using the Click router
- Standalone solution, no integration with existing MANO software

EmuStack
--------

Combination of OpenStack with Linux TC

- OpenStack configured to execute Containers
- Not made available as Open Source
- Still requires complex OpenStack installation

vim-emu
-------

Lightweight emulation of OpenStack API

- Fakes the OpenStack API, but launches lightweight containers
- Based on `Containernet <https://containernet.github.io/>`_ which is based on `Mininet <http://mininet.org/>`_
- Can emulate 100's of OpenStack installations on developer hardware

vim-emu
-------
.. image:: figures/architecture-vim-emu.svg
    :width: 100%

vim-emu
-------
.. code:: python

    self.net = DCNetwork()
    self.dc1 = self.net.addDatacenter("dc1")
    self.dc2 = self.net.addDatacenter("dc2")
    self.net.addLink(self.dc1, self.dc2,
                     cls=TCLink, delay="50ms")
    self.api1 = OpenstackApi("0.0.0.0", 6001)
    self.api1.connect_datacenter(self.dc1)
    self.api2 = OpenstackApi("0.0.0.0", 6002)
    self.api2.connect_datacenter(self.dc2)

Open Issues
-----------

.. class:: columns

    .. class:: column
        :width: 80%

    - ``vim-emu``'s network emulation is lacking
    - Charm configurations cannot access isolated environment
    - Emulation of Service Function Chaining not possible
    - Only covers VIM layer

    .. class:: column
        :width: 20%

    .. image:: figures/exclamation.svg
        :width: 100%

Implementation
==============

Isolated environment
--------------------
.. image:: figures/isolated-environment.svg
    :width: 100%
    
Floating-IPs
------------
.. image:: figures/floating-ips.svg
    :width: 100%

Floating-IPs reality
--------------------
.. image:: figures/floating-ips-real.svg
    :width: 100%

Floating-IP solution
--------------------

.. code:: bash

    $ docker inspect mn.d1 \
        -f '{{range .NetworkSettings.Networks}}
            {{.IPAddress}}{{end}}'
    172.17.0.2

Difference to Floating-IPs:
    Single IP per instance, no IP per virtual network port

Service Function Chaining
-------------------------
.. image:: figures/service-function-chaining.svg
    :width: 100%

Service Function Chaining implementation
----------------------------------------
.. image:: figures/service-function-chaining-implementation.svg
    :width: 100%

Service Function Chaining fixes
-------------------------------


.. class:: columns

    .. class:: column
        :width: 80%

    - Fixed direction of forwarding rules
    - Fixed forwarding between Port Pairs
    - Added support for classifiers
    
    .. class:: column
        :width: 20%

    .. image:: figures/checked-list.svg
        :width: 100%


Situation after fixes
---------------------
.. class:: columns

    .. class:: column
        :width: 50%

        .. image:: figures/development-workflow-vim-emu-generated.svg
            :width: 110%

    .. class:: column
        :width: 50%

    - Processes accelerated using containers
    - Fundamental workflow stays the same
    - Problems are still mostly the same


Full-Stack-Emulation (high-level API)
------------------------------------------------

.. code:: python

    with PreConfiguredOSM() as osm:
        osm.onboard_vnfd('vnfd/')
        nsd_id = osm.onboard_nsd('nsd/')
        ns_id = osm.ns_create('ns_name', nsd_id)
        osm.ns_wait_until_all_in_status('running')
        # confirm that the NS works...
        osm.ns_delete(ns_id)

Full-Stack-Emulation (mid-level API)
------------------------------------------------

.. code:: python

    api1 = OpenstackApiEndpoint("0.0.0.0", 6001)
    api2 = OpenstackApiEndpoint("0.0.0.0", 6002)
    osm = OSM(net)
    # ...
    vim1 = osm.register_emulated_vim('vim1', api1)
    vim2 = osm.register_emulated_vim('vim2', api2)
    # ...
    osm.ns_create('ns1', nsd1, vim1)
    osm.ns_create('ns2', nsd2, vim2)

----------------

.. image:: figures/osm-architecture-generated.svg
    :height: 108%

----------------

.. image:: figures/osm-architecture-highlighted-generated.svg
    :height: 108%

Full-Stack-Emulation (low-level API)
------------------------------------------------

.. code:: python

    s1 = net.addSwitch('s1')
    zookeeper = Zookeeper(net, zookeeper_ip)
    kafka = Kafka(net, kafka_ip, zookeeper_ip)
    mongo = Mongo(net, mongo_ip)
    nbi = NBI(net, nbi_ip, mongo_ip, kafka_ip)
    ro_db = Mysql(net, ro_db_ip)
    # ...
    net.addLink(kafka, s1)
    # ...

Challenges
----------

.. class:: columns

    .. class:: column
        :width: 80%

    - Fix unreliable startup of OSM components
    - Make containers compatible with Containernet
    - (Make Containernet compatible with Containers)
    - Design simple, yet powerful API

    .. class:: column
        :width: 20%

    .. image:: figures/exclamation.svg
        :width: 100%

Live Demo
---------

.. image:: figures/live-demo.svg
    :width: 100%


Experiments
===========

Juju Charms
-----------

.. image:: figures/plot-charms.pdf
    :width: 100%

Service Function Chaining (fixed)
---------------------------------

.. image:: figures/plot-sfc-good-bps.pdf
    :width: 100%

Full-Stack-Emulation - Startup time
-----------------------------------

.. image:: figures/plot-startup.pdf
    :width: 100%


Full-Stack-Emulation - Parallel deploy
--------------------------------------

.. image:: figures/plot-parallel-deploy.pdf
    :width: 100%

Full-Stack-Emulation - Slow Deploy
----------------------------------

.. image:: figures/plot-cpu.pdf
    :width: 100%

Full-Stack-Emulation - Slow Deploy
----------------------------------

.. image:: figures/plot-ram.pdf
    :width: 100%


Live Demo
---------

.. image:: figures/live-demo-finish.svg
    :width: 100%

Conclusion
==========

Where can this be useful?
-------------------------

.. class:: columns

    .. class:: column
        :width: 80%

    - Better first-time experience
    - Continuous Integration
    - Experiments with large number of VNFs
    - Experiments with changes of the internal OSM topology
    - Testing multiple OSM instances/versions in parallel

    .. class:: column
        :width: 20%

    .. image:: figures/checked-list.svg
        :width: 100%


Submitted Fixes
---------------

**Everything (relevant) merged upstream**

+--------------+-+--------+--------+
| project      | | opened | merged |
+==============+=+========+========+
| vim-emu      | | 32     | 31     |
+--------------+-+--------+--------+
| Containernet | | 5      | 5      |
+--------------+-+--------+--------+
| RO           | | 2      | 2      |
+--------------+-+--------+--------+
| osm-client   | | 1      | 0      |
+--------------+-+--------+--------+

Future work
-----------
.. class:: columns

    .. class:: column
        :width: 80%

    - Extensions to the API
    - Further enhancement of OpenStack compatibility
    - Split into separate library
    - Libvirt support

    .. class:: column
        :width: 20%

    .. image:: figures/future-work.svg
        :width: 100%


Thanks to
---------
- Manuel Peuster for supervision
- https://github.com/Xaviju/inkscape-open-symbols for the icons used in the illustrations

Thanks to
---------

.. image:: figures/review-party-cropped.jpg
    :width: 100%

all attendees of the *review party*

Thanks for
----------

.. image:: figures/barcelona-cropped.jpg
    :width: 100%

the opportunity to attend the OSM Hackfest in Barcelona

Questions?
----------

.. image:: figures/questions.svg
    :width: 100%
