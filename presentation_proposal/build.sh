#!/bin/bash
set -xe

pandoc \
    -s \
    -t beamer \
    -o "../build/Presentation Proposal Master Thesis Erik Schilling.pdf" \
    presentation.rst \
    --toc \
    --metadata theme=simple \
    --metadata fontsize=17pt \
    --metadata section-titles=false \
    --metadata aspectratio=169 \
    --metadata links-as-notes=true
