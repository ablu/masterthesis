#!/bin/bash
set -xe


for i in $(find -name "*.dia")
do
    BASENAME=`echo $i | sed 's/\.dia$//'`

    dia "$i" -t svg -e "$BASENAME-generated.svg"
done

pandoc \
    -s \
    -t beamer \
    -o "../build/Presentation Defense Master Thesis Erik Schilling.pdf" \
    presentation.rst \
    --toc \
    --metadata section-titles=false \
    --metadata theme=simple \
    --metadata fontsize=17pt \
    --metadata aspectratio=169 \
    --metadata links-as-notes=true
