library(tidyverse)
library(gmodels)

plot_width <- 6
plot_height <- 3.5


d = read.csv("charms/merged.csv") %>%
  rename(`NS creation`=ns_create, `NS awaiting\nconfiguration`=charm_deployment_start,
         `Juju waiting\nfor machine`=waiting_for_machine, `Juju installing\ncharm software`=installing_charm_software,
         `NS action`=ns_action, `NS deletion`=ns_delete) %>%
  gather(Var, Value) %>%
  group_by(Var) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )


ggplot(d, aes(x=Var, y=mean, ymin=low, ymax=high)) +
  geom_col(position="dodge", width=0.2, fill="#F8766D") +
  geom_errorbar(position="dodge", width=0.1, size=0.2) +
  scale_x_discrete(name="Status of NS") + 
  scale_y_continuous(name="Mean time in status [s]") +
  theme_bw()
ggsave('../thesis/figures/plot_charms.eps', width=plot_width, height=plot_height)
