\chapter{Experiments}
\label{sec:experiments}

In order to provide a better understanding about the possibilities and flexibility of the developed solution, experiments have been conducted which test the viability of \texttt{vim-emu} as a prototyping platform.
The general setup of the experiments is explained in \Cref{sec:experiment-setup}, followed by the experiments for Charms (\Cref{sec:experiment-charm}), Forwarding Graphs (\Cref{sec:vnffg-experiment}) and the Full-Stack-Emulation (\Cref{sec:experiment-full-stack}).

\section{Setup}
\label{sec:experiment-setup}
Due to the great reproducibility of the full-stack emulation developed in \Cref{sec:full-stack-emulation}, all tests were developed as \texttt{vim-emu} emulation scripts.
The scripts perform their measurements and log the results into \ac{CSV} files which are then plotted using \texttt{ggplot}\footnote{https://ggplot2.tidyverse.org/reference/ggplot.html}. The full data set and plotting scripts can be found on the DVD (\Cref{chap:dvd}) attached to this thesis.
All experiments have been executed in a \ac{VM} with 16GB of RAM, running on an Intel Xeon E5-2695 v3 2.30GHz CPU of which 8 cores have been assigned to the \ac{VM}.

Since measurements within the scripts are usually performed by polling the \ac{NBI} \ac{API} some technical restrictions to the precision of the data apply.
First, \ac{OSM} itself internally uses polling in order to update the state of certain life-cycle events.
For example, in order to determine the successful deployment of a \ac{VNF}, the launched \ac{VM} instances are polled by the \ac{RO} component which updates its own database. \ac{LCM} then also polls \ac{RO} every five seconds in order to update the status in the common database of \ac{LCM} and \ac{NBI}.
Together this could lead to delays of up to ten seconds between the actual finish of the deployment until the \ac{API} reports the up-to-date deployment status.
On top of that, the emulation scripts will also perform a polling of the \ac{NBI} \ac{API} in order to determine the state.
In the following experiments this polling is performed every second.

While the polling leads to some inaccuracies, measuring the theoretical deployment length by, for example, parsing the logs of \ac{OSM}, would not give a realistic impression of the time a developer would need to wait in order to evaluate a change.
In order to counter these inaccuracies, all experiments have been repeated multiple times and error bars which indicate a 95\% confidence interval for the mean are given.
Still, the precision of the measurements are limited by the one second polling delay of the \ac{NBI} \ac{API} which, especially for short actions, may lead zero-width confidence intervals if the action always finishes after the same number of polls.
However, the main target of the measurements is to provide a general idea about the performance of the solution, which should not be hindered by these slight inaccuracies.

\section{Deployment time of Juju Charms}
\label{sec:experiment-charm}
When testing the Charm to \ac{VNF} interaction, usually multiple deployments are required while designing a Charm.
Therefore, a fast re-deployment of a Charm can help to speed up the feedback cycle while trying new iterations of a Charm.

In order to get an idea about the time spent in the different lifecycle states of a Charm, an experiment was designed which waits for the major configuration statuses and measures the time between them.

A simplified version of the developed emulation script is given in \Cref{lst:charm-experiment}, but the full emulation script was also submitted as \texttt{vim-emu} example under Changeset~7327\footnote{\url{https://osm.etsi.org/gerrit/7327}}.
The code onboards an example \ac{VNFD} (\Cref{lst:charm-experiment-vnfd}) and \ac{NSD} (\Cref{lst:charm-experiment-nsd}) and instantiates the \ac{NS}.
Here, the \ac{NS} will first enter the ``operational-status'' \texttt{running} state which indicates that the \ac{VNF} has been deployed to the emulated \ac{VIM}.
After this, \ac{LCM} triggers a deployment of the Charm and keeps the ``configuration-status'' in sync with the status reported by Juju.
The script will measure the time spent in the states ``waiting for machine'' and ``installing charm software''.
The first state indicates that Juju is allocating and preparing a new \ac{LXC} container for the Charm execution.
After the provisioning of the machine, the ``installing charm software'' indicates the installation of the actual Charm within the container.
The state ``Ready!'' concludes the Charm deployment and also indicates that the Charm finished the configuration of the \ac{VNFD}.

After this ``day-0'' configuration was performed, the emulation script triggers a manual action execution by calling \texttt{ns\_action()}.
Since the \ac{API} of \ac{OSM} does not return any information about the execution of the action, the execution time has to be measured by polling the state of the container itself.
This is done by repetitively attempting to output the file which is expected to be created by \texttt{cat /testmanual}.
As long as the file is not created yet the command will output an error.
Only as soon as the Charm issued a \texttt{touch /testmanual}, the file exists and \texttt{cat} returns an empty string.
Since the command execution has a lot less overhead compared to calling the \ac{API}, here the polling is done every 100ms instead of every second.
As the last step, the \ac{NS} is deleted, which also triggers a deletion of the Charm.

\begin{lstlisting}[caption={Simplified version of the emulation script used for the Charm experiment. For brevity the time measurement statements have been left out. The script onboards an example \ac{VNFD} and \ac{NSD} and deploys them as \ac{NS}. After the configuration is finished, a manual configuration action is triggered using \texttt{ns\_action()} and the result is awaited. Afterwards the \ac{NS} is deleted again.}, label=lst:charm-experiment,float]
osm.onboard_vnfd('../vnfs/simple_charmed_vnfd')
nsd_id = osm.onboard_nsd('../services/simple_charmed_nsd')
ns_id = osm.ns_create('charmed-ns-%d' % n, nsd_id)
osm.ns_wait_until_all_in_status('running')

wait_for_detailed_configuration_status(osm, 'waiting for machine')
wait_for_detailed_configuration_status(osm, 'installing charm software')
wait_for_detailed_configuration_status(osm, 'Ready!')

instance = osm.api.compute.find_server_by_name_or_id( 'dc1_charmed-ns-%d-1--1' % n).emulator_compute
osm.ns_action(ns_id, 1, 'touch')
while instance.cmd('cat /testmanual') != '':
    time.sleep(0.1)

osm.ns_delete(ns_id)
osm.ns_wait_until_all_in_status('terminated')
\end{lstlisting}

The result of 20 executions of the experiment is plotted in \Cref{fig:charm-experiment}.
While the creation of the \ac{NS} on the \ac{VIM} is faster than 8 seconds, the complete deployment of the Charm requires over 2 full minutes.
Even the execution of a simple primitive requires approximately the same time as the creation of the \ac{NS}.
Finally, the deletion of the \ac{NS} together with the Charm also is quite slow with an estimated mean between 80 and 110 seconds.
Later tests without Charms revealed a lot faster deletion time, which indicates that a majority of the time is spent waiting for Juju to delete the Charm.

\begin{figure}
    \centering
    \includegraphics{figures/plot_charms.eps}
    \caption{Time spent in the different lifecycle states across 20 test runs during the Charm deployment experiment.}
    \label{fig:charm-experiment}
\end{figure}

As a result, given the slow Juju deployment, \texttt{vim-emu}'s faster \ac{NS} deployment only provides a small advantage while developing Charms.
However, the ability to describe the complete testing process using a reproducable, easily maintainable script reduces the number of manual interaction and allows to automatic verification of Charms during, for example, continuous integration testing.

Further attempts have been made to test the deployment of \acp{VNFD} with a \ac{VM} count larger than one, however, the testing revealed that \ac{OSM} only configures the first \ac{VNF} instance in such a scenario.
This bug has been filed upstream\footnote{\url{https://osm.etsi.org/bugzilla/show_bug.cgi?id=651}} and is a great example of how simplifications to large scale testing could have helped to detect an issue earlier.


\section{Deployment time and throughput of Forwarding Graphs}
\label{sec:vnffg-experiment}
The second large feature of this thesis was to allow the emulation of \acp{VNFFG}.
While the testing in \Cref{sec:vnffg-impl} proved the general functionality of \acp{VNFFG}, no quantitative performance measurements have been conducted.
This is done now by designing an experiment which deploys chains with increasing length and measuring the throughput and \ac{RTT} of TCP connections.

Since the number of nodes in a \ac{VNFFG} is defined in the \ac{NSD}, a dynamic generation of the descriptor is required for full automation of the test.
For this the templating engine \texttt{jinja2}\footnote{\url{http://jinja.pocoo.org/docs/2.10/}} was used in combination with a template given in \Cref{lst:vnffg-chain-template}.
Here the values \texttt{n} and \texttt{subnet} are used in order to configure the number of nodes and subnet of the graph.
The subnet needs to be configured for each run since \texttt{vim-emu} assigns a new subnet to each network, but does not reuse subnets after an emulated network is destroyed.

The \ac{NSD} describes a forwarding graph where the traffic of a \ac{VNF} is routed through \texttt{n} successive nodes.
The classifier only matches the traffic targeting port 80 of the last IP in the forwarding graph which allows to connect to other ports for comparing a direct connection with connections through the forwarding graph.
In the case \texttt{n=1} the deployed \ac{NS} will only consist of the traffic generation \ac{VNF} and the target node.
Since no intermediate nodes exist in that scenario, the forwarding path and the normal routing path are identical.
Finally, only the traffic from source to target is rerouted, the response packets are not matched and thus flow along the direct path.
\Cref{fig:vnffg-experiment-illustration} illustrates the generated forwarding graph.
The emulation script was submitted as Changeset~7329\footnote{\url{https://osm.etsi.org/gerrit/7329}} in order to serve as an example.

\begin{figure}
    \centering
    \includegraphics[width=0.7\columnwidth]{figures/experiment-vnffg-illustration.pdf}
    \caption{Illustration of the traffic flow within the generated \ac{VNFFG}. The direct traffic skips the intermediate hops and is directly forwarded to the target. In contrast, the traffic which is matched by the classifier is routed through the increasing number of intermediate hops.}
    \label{fig:vnffg-experiment-illustration}
\end{figure}

During the experiment \texttt{iperf3}\footnote{\url{https://iperf.fr/}} servers will be launched on the ports 80 and 81 on the last node of the chain.
Then, on the traffic generation node, a measurement of the forwarding graph is initiated.
This is done by executing \texttt{iperf3 -J -t 30 -c <targetip> -p 80} which runs a troughput test for 30 seconds targeting port 80 of the last node while outputting the result as \ac{JSON}.
Since the test uses TCP, the classifier of the forwarding graph will match and the traffic is routed through the \texttt{n-1} intermediate notes before arriving at the destination.
After the test finishes, a second measurement is done with target port 81 but an otherwise identical \texttt{iperf3} invocation.
Here, the classifier does not match and the traffic will be directly routed to the destination without any intermediate hops.
For both tests the \ac{JSON} output is evaluated by reading the summary values reported by \texttt{iperf3} in the \texttt{"end"} field.
Specifically, the values \texttt{['end']['streams'][0]['sender']['mean\_rtt']} and \texttt{['end']['sum\_received']['bits\_per\_second']} are read and written to a \ac{CSV} file.
Additionally, the time spent in the deployment and deletion phase was collected.

The experiment was then executed for $n \in [1,15]$ while being repeated 15 times.
\Cref{fig:vnffg-deployment-plot} shows the result for the deployment and deletion time.
Unsurprisingly, the deployment and deletion time both raise with the number of involved nodes.
However, a deployment time around 43s and a deletion time of approximately 3s for \texttt{n=15} is pretty good, especially given that an OpenStack deployment with 15 \acp{VM} would not have been possible on the used environment at all.

Contrary to that, the measured throughput in \Cref{fig:vnffg-bps-bad-plot} was unexpected.
With the emulation of an increasing number of nodes the throughput along the forwarding graph was expected to either decline or at least stay on a consistent level.
In spite of that the plot only shows a decline until \texttt{n=5} (with a high error margin) and an otherwise mostly identical throughput compared to the direct connection.
A deeper analysis revealed that the deployment time, as reported by the \ac{OSM} \ac{API} and plotted in \Cref{fig:vnffg-deployment-plot}, does not include the installation of forwarding rules.
This resulted in \texttt{iperf3} being invoked before the forwarding graphs have been installed.
Since no other \ac{API} method is available to track the status of installed forwarding rules, a bug\footnote{\url{https://osm.etsi.org/bugzilla/show_bug.cgi?id=653}} was filed against \ac{OSM}.
Again, larger scale testing capabilities could have helped to find such issues earlier.

After adding additional wait time until the forwarding graph is initiated the experiment was repeated, resulting in \Cref{fig:vnffg-bps-plot}.
Here the expected decrease in throughput with an increased forwarding graph length is nicely visible.
Starting from around 3.2GiB/s the throughput is observed to drop around 1GiB/s in this simple forwarding example.
For prototyping purposes this performance probably is enough, however, given the lack of openly available standard \acp{VNF} it is hard to reason about the actually required throughput capabilities for performing reasonable emulations.

Finally, \Cref{fig:vnffg-rtt-plot} shows the measured \ac{RTT} as reported by \texttt{iperf3}.
Due to retransmissions the \ac{RTT} accuracy of the plot suffers from runaway values and the low sample size.
The combination of runaway values and low sample size also makes the application of t-distribution confidence intervals questionable, but in general the mean \ac{RTT} stays under 70$\upmu$s.

\begin{figure}
    \centering
    \includegraphics{figures/plot_vnffg_deploy_delete.eps}
    \caption{Duration of \ac{NS} deployment and deletion during the \ac{VNFFG} experiment as reported by \ac{OSM}.}
    \label{fig:vnffg-deployment-plot}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[scale=1]{figures/plot_vnffg_bps_bad.eps}
    \caption{Throughput measured during the experiment before ensuring finished \ac{VNFFG} creation.}
    \label{fig:vnffg-bps-bad-plot}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[scale=1]{figures/plot_vnffg_bps_good.eps}
    \caption{Throughput measured during the experiment after ensuring finished \ac{VNFFG} creation.}
    \label{fig:vnffg-bps-plot}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[scale=1]{figures/plot_vnffg_rtt.eps}
    \caption{\ac{RTT} during the \ac{VNFFG} experiment as measured by \texttt{iperf3}.}
    \label{fig:vnffg-rtt-plot}
\end{figure}


\section{Startup time and limit testing of Full-Stack Emulation}
\label{sec:experiment-full-stack}
After measuring aspects related to Charms or \acp{VNFFG} in the previous sections, this section describes the experiments executed in order to provide more insight into the practicability of the solution developed in \Cref{sec:full-stack-emulation}.

\subsubsection{1. Parallel launch of \acp{NS}}

As a first experiment a simple stress test is performed.
Here, the environment provided by \texttt{PreConfiguredOSM} is used in order to deploy the \texttt{pingpong} example of \texttt{vim-emu} an increasing number of times.
The deployments are being done in parallel before the script waits until all deployments have completed.
After completion, all deployments are removed in parallel and the script waits until the deletions succeeded.
The deployment and deletion time is measured together with the general startup and exit time of the environment, as well as the onboarding time of loading the \ac{NS} and its two \acp{VNFD}.
Again, the developed emulation script was submitted as Changeset~7328\url{https://osm.etsi.org/gerrit/7328}

After 10 executions, \Cref{fig:parallel-deploy-plot} shows that while the deployment and deletion time is raising linearly for a small number of parallel \acp{NS}, irregularities occur for larger numbers.
These irregularities can be explained by observing that some deployments start to fail around \texttt{n=15} in \Cref{fig:parallel-deploy-num-failed-plot}.
The reason for these failures is that \ac{OSM} registers a timeout for each \ac{NS} after which a deployment is declared as failed.
With too many parallel deployments happening an increasing amount of deployments are exceeding this timeout, resulting in the reported failures.

\begin{figure}
    \centering
    \includegraphics{figures/plot_ping_pong_parallel_deploy_and_delete.eps}
    \caption{Deployment duration while doing a parallel deployment of \texttt{n} \acp{NS}.}
    \label{fig:parallel-deploy-plot}
\end{figure}
\begin{figure}
    \centering
    \includegraphics{figures/plot_ping_pong_parallel_num_failed.eps}
    \caption{Number of failed \acp{NS} while performing a parallel deployment of \texttt{n} services at the same time.}
    \label{fig:parallel-deploy-num-failed-plot}
\end{figure}

As numbers being independent of the number of deployed \acp{NS}, the start of the environment was measured to take 43s in the mean while the destroying of an empty environment (after deleting all \acp{NS}) took 4.2s in the mean.
Onboarding the two \acp{VNFD} and the \ac{NS} took 1.8s on average.

Here, the most limiting factor is the startup time of the environment.
A \ac{VNF} developer would have to wait over 40s for the launch of the platform while the actual deployment of a smaller number of services only takes a couple of seconds.
However, this situation could be improved by writing an emulation script which keeps the environment running and only reloads the \acp{NSD} and \acp{VNFD} when necessary.
In that case the environment would work similar to a normal \ac{OSM} install, except being a bit more lightweight due to the reduced number of executed components and being easier to reason about a finished launch process of \ac{OSM}.
Furthermore, since \texttt{vim-emu} waits until the dependencies of each component are fulfilled, the startup time is more predictable compared to the Docker Swarm based launch of an ordinary \ac{OSM} install where the components restart until their dependencies become available.

Still, in order to get a better idea about where the long startup time is generated a more detailed measurement was performed by measuring the startup time of the individual components independently from each other (submitted as Changeset~7348\footnote{\url{https://osm.etsi.org/gerrit/7348}}).
The result of 150 launches of each component is given in \Cref{fig:startup-component-breakdown}, but needs to be analyzed carefully.
The reported startup time of zero seconds for the services Mongo, MySQL and Zookeeper does not mean that the services start instantaneously, but that the \texttt{start()} function of that service does not block until the service is available.
This means that, for example, the startup time of Zookeeper is also included within the reported time for Kafka, because Kafka waits until a connection to Zookeeper is possible before opening its own port.
The same is the case for the \ac{RO} component, which also includes the startup time of its database in its reported number.
While the startup of the actual MySQL service is fast here, \ac{RO} performs a full initialization of the database before opening its own port.
This analysis indicates that the startup process could be further accelerated by using a pre-initialized database of \ac{RO}.
Additionally, consulting \Cref{fig:osm-architecture} reveals that the \ac{RO} component does not have any other dependencies apart from the database.
Therefore \ac{RO} could be launched in parallel to Kafka, Zookeeper and \ac{NBI}.
Depending on the available number of CPU cores on the host machine this has the potential to further speed up the start process.

\begin{figure}
    \centering
    \includegraphics{figures/plot_start_breakdown.eps}
    \caption{Time required to invoke the \texttt{start()} function of each component.}
    \label{fig:startup-component-breakdown}
\end{figure}


\subsubsection{2. Increasing delay between \ac{OSM} components}
In order to demonstrate the capability of analyzing the impact of topology changes using the developed solution, the second experiment adds increasing delay to the network links between the \ac{OSM} components and measures the startup time of the components and the deployment and deletion of the \texttt{pingpong} example.
Starting with a delay of 0ms, each round the delay is increased by 10ms until the system fails to start within 15 minutes.
The emulation script was submitted as Changeset~7347\footnote{\url{https://osm.etsi.org/gerrit/7347}}.

Since, as illustrated by \Cref{fig:delay-illustration}, the components are connected using a switch and a link represents a connection between a component and the switch, a delay of 10ms on each link will result in a total delay of 20ms when communicating from one component to another due to two links being involved.
Therefore, a full \ac{RTT} would be 40ms, a value which is confirmed by measuring the \texttt{ping} time between the \ac{RO} component and its database (\Cref{fig:delay-ping-plot}).

\begin{figure}
    \centering
    \includegraphics[width=0.4\columnwidth]{figures/delay-illustration.pdf}
    \caption{Illustration of the delay between two components. Since two links are involved for each connection, the total \ac{RTT} is four times the delay of the individual link.}
    \label{fig:delay-illustration}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{figures/plot_delay_ping.eps}
    \caption{Ping \ac{RTT} between \ac{RO} and its MySQL database during the link delay experiment.}
    \label{fig:delay-ping-plot}
\end{figure}

The impact on the startup time of the components and the deployment and deletion of \acp{NS} is presented in \Cref{fig:delay-plot} for 10 repetitions.
While \ac{OSM} itself does not perform any computationally challenging tasks, the communication overhead between the components raises linearly with an increased link time.
During the 10 runs of the experiment one run aborted at a link delay of 110ms, five runs at 130ms and the other four runs at 140ms.
Here, Kafka's connection to Zookeeper timed out in which case no attempts for reconnection are performed, blocking the launch of the other components.

\begin{figure}
    \centering
    \includegraphics{figures/plot_delay.eps}
    \caption{Time required for the startup of \ac{OSM} and the deployment and deletion of the \texttt{pingpong} example during the link delay experiment.}
    \label{fig:delay-plot}
\end{figure}


\subsubsection{3. Limit testing}
Since the parallel launching of \acp{NS} quickly leaded to timeouts, an experiment with a more moderate approach of creating many server instances was performed with the target to test the limits of the system.

The experiment, again, deploys an increasing number of \acp{NS}, but waits until each service is fully deployed before deploying the next one.
Each \ac{NS} conists of a \ac{VNFD} which defines \texttt{count:~10} in its \texttt{vdu} descriptor, triggering a deploy of 10 instances per \ac{NS} deployment.
During the experiment, after each deployment was reported as complete, the CPU and RAM usage was measured using the Python package \texttt{psutil}\footnote{\url{https://psutil.readthedocs.io/en/latest/}}.
Therefore, the measured numbers capture the ``idle'' load during which no new services were added.
The number of \acp{NS} in each test run was increased until a deployment was reported as failed, at which point the run aborted.
Again, the emulation script was submitted upstream in Changeset~7360\footnote{\url{https://osm.etsi.org/gerrit/7360}} to serve as an example.
The complete experiment consisted of 10 test runs.

During the execution of 10 experiments, each run failed during deploying the 15th \ac{NS}, resulting in at least 140 instances being launched in each test run.
This number is lot lower than what would could be expected from a container based solution.
The RAM usage, as reported in \Cref{fig:limit-ram-plot}, also indicates that the system still had plenty of resources available.
Here, the complete testing system with operating system and emulation script never exceeded 1GB, leaving most of the RAM unused.
However, the measured CPU usage (\Cref{fig:limit-cpu-plot}) gives a hint regarding the observed bottleneck.
The measurements reveal a steep jump in the CPU usage after deploying the third \ac{NS}.
After this jump, the CPU usage stays on the same level regardless of the increased number of \acp{NS}.

While no more detailed analysis was possible, the observed behaviour could be explained by \ac{RO} no longer being able to perform its periodic \ac{VM} status refresh at around 20 to 30 \ac{VNF} instances (2 to 3 \acp{NS}).
After each refresh \ac{RO} queues a new one before starting to process other outstanding refreshes.
If now the queue of outstanding refreshes cannot be drained before the newly queued refresh would be pending, effectively an endless loop is created.
Since the \ac{RO} component operates in a single thread, this would match to the observation that the CPU usage never significantly exceeds the percentage that would be expected by a single CPU core running at 100\%.
Additionally, while not measured by the emulation script output, the logs of the experiment runs revealed that the deployment time of the first \acp{NS} was a lot faster compared to the later iterations of the emulation.
Here, \ac{RO} uses a shorter polling delay until an instance is being reported as launched, further increasing the queue length.

Therefore, further experiments are required to test the hypothesis above, for example by increasing the refresh interval in \ac{RO} or measuring the queue length of outstanding actions.
However, the experiments prove that the developed solution is a great tool for finding scalability issues in the \ac{MANO} layer which are otherwise hard to produce using development hardware.


\begin{figure}
    \centering
    \includegraphics{figures/plot_limit_ram.eps}
    \caption{RAM usage during the limit testing experiment.}
    \label{fig:limit-ram-plot}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{figures/plot_limit_cpu.eps}
    \caption{CPU usage during the limit testing experiment.}
    \label{fig:limit-cpu-plot}
\end{figure}