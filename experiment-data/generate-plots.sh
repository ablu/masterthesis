#!/bin/bash
command -v Rscript || exit 0 # CI does not have R
find -name "*.r" | xargs -P8 -n1 Rscript
