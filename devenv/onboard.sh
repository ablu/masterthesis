#!/bin/bash

set -xe

cd /vagrant/examples/
for i in $(ls -d *-docker/|sed 's/-docker\///g')
do
    sudo docker build -t $i ${i}-docker/
done

for i in $(ls -d *-nsd/ *-vnfd/|sed 's/\///g')
do
    tar czf $i.tar.gz $i/
done

echo "onboarding VNFs"
for i in $(ls *vnfd.tar.gz)
do
    osm vnfd-create $i
done

echo "onboarding NSs"
for i in $(ls *nsd.tar.gz)
do
    osm nsd-create $i
done

osm vnfd-list
osm nsd-list