library(tidyverse)
library(gmodels)

plot_width <- 6
plot_height <- 3.5

d = read.csv("vnffg/1/merged.csv") %>%
  rename(Deployment=deploy, Deletion=delete) %>%
  rename(`VNFFG`=vnffg_iperf_bps, `Direct`=normal_iperf_bps) %>%
  gather(Var, Value, -n) %>%
  group_by(n, Var) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )
d2_bps = read.csv("vnffg/2/merged.csv") %>%
  rename(`VNFFG`=vnffg_iperf_bps, `Direct`=normal_iperf_bps) %>%
  gather(Var, Value, -n) %>%
  group_by(n, Var) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )
d2_rtt = read.csv("vnffg/2/merged.csv") %>%
  rename(`VNFFG`=vnffg_iperf_rtt, `Direct`=normal_iperf_rtt) %>%
  gather(Var, Value, -n) %>%
  group_by(n, Var) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )


deploy_delete <- d
deploy_delete$Var <- factor(deploy_delete$Var, levels=c("Deployment", "Deletion"))

chain_length_label <- "Number of VNFs in forwarding chain"

width <- 0.9
position=position_dodge(width=width)
ggplot(filter(deploy_delete, !is.na(Var)), aes(x=n, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name=chain_length_label, breaks=seq(1, 15, 1)) + 
  scale_y_continuous(name="Mean duration [s]") +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_vnffg_deploy_delete.eps', width=plot_width, height=plot_height)

iperf_rtt = d2_rtt
iperf_rtt$Var <- factor(iperf_rtt$Var, levels=c("VNFFG", "Direct"))
width <- 0.9
position=position_dodge(width=width)
ggplot(filter(iperf_rtt, !is.na(Var)), aes(x=n, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name=chain_length_label, breaks=seq(1, 15, 1)) + 
  scale_y_continuous(name="Mean RTT [us]") +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_vnffg_rtt.eps', width=plot_width, height=plot_height)

iperf_bps = d
iperf_bps$Var <- factor(iperf_bps$Var, levels=c("VNFFG", "Direct"))
factor <- 8 * 1024 * 1024 * 1024 # GB
iperf_bps$mean <- iperf_bps$mean / factor
iperf_bps$low <- iperf_bps$low / factor
iperf_bps$high <- iperf_bps$high / factor
width <- 0.9
position=position_dodge(width=width)
ggplot(filter(iperf_bps, !is.na(Var)), aes(x=n, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name=chain_length_label, breaks=seq(1, 15, 1)) + 
  scale_y_continuous(name="Mean throughput [GiB/s]") +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_vnffg_bps_bad.eps', width=plot_width, height=plot_height)

iperf_bps2 = d2_bps
iperf_bps2$Var <- factor(iperf_bps2$Var, levels=c("VNFFG", "Direct"))
factor <- 8 * 1024 * 1024 * 1024 # GB
iperf_bps2$mean <- iperf_bps2$mean / factor
iperf_bps2$low <- iperf_bps2$low / factor
iperf_bps2$high <- iperf_bps2$high / factor
width <- 0.9
position=position_dodge(width=width)
ggplot(filter(iperf_bps2, !is.na(Var)), aes(x=n, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name=chain_length_label, breaks=seq(1, 15, 1)) + 
  scale_y_continuous(name="Mean throughput [GiB/s]") +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_vnffg_bps_good.eps', width=plot_width, height=plot_height)