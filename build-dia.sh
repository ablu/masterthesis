#!/bin/bash

set -xe

BASENAME=`echo $1 | sed 's/\..*$//'`

dia "$1" -t svg -e "$BASENAME.dsavg"
rsvg-convert "$BASENAME.dsavg" -f pdf -o "$BASENAME.pdf"
rm "$BASENAME.dsavg"


