\section{Support for \acf{SFC}}
Next to the configuration using Charms, \acf{SFC} or, as called in the context of \ac{OSM}, \ac{VNFFG} is another mechanism to compose specialized services from standard \acp{VNF}.
However, while using Charms, \acp{VNF} need to be explicitly configured in order to compose a larger service, \acs{VNFFG} allows a transparent composition of services.
This is achieved by changing the default course of packets within a network.
For example, instead of configuring the source to always send all traffic through a firewall, the source will directly issue connections to its final destination.
The defined forwarding rules of an \ac{NSD} then can reroute the traffic through a firewall where desired.
Since the forwarding is transparent to the involved parties it does not require any configuration of the \acp{VNF} itself but only to their surrounding routers.


\subsection{Use Case Example}
In order to demonstrate the forwarding graph feature this work will rely on two examples.

The first example will be a simple \ac{VNF} chain through which a single flow is routed.
The chain contains three \acp{VNF}, each with a single interface, where the traffic of the first \ac{VNF} will be forwarded through the second and third, as illustrated in \Cref{fig:vnffg-simple-example}.
The first \ac{VNF} will generate traffic by initiating a TCP connection to the third \ac{VNF} using \texttt{nc}\footnote{\url{https://nmap.org/ncat/}}.
If no forwarding graphs are defined, the traffic should go from VNF1 to VNF3 directly and VNF2 will not see any traffic.
However, if a forwarding graph is defined which matches the TCP flow and redirects it to VNF2, running \texttt{iftop} or \texttt{tcpdump} within that \ac{VNF} instance should allow a simple confirmation that the forwarding graph is working.
The relevant parts of the \ac{NSD} for this example are given in \Cref{lst:vnffg-example-nsd} and will be used throughout this section.

\begin{figure}
    \centering
    \def\svgwidth{0.4\columnwidth}
    \input{figures/simple-vnffg-example.pdf_tex}
    \caption{Illustration of the simple \ac{VNFFG} example.}
    \label{fig:vnffg-simple-example}
\end{figure}

A second example (\Cref{fig:vnffg-complex-example}) will be used to verify the support of multiple interfaces and forwarding graphs.
Here, six \acp{VNF} will be launched with forwarding rules according to \Cref{fig:vnffg-complex-example}.
The outgoing traffic of a single \ac{VNF} is sent to two different \acp{VNF} based upon three classifiers.
For the first and second flow, these \acp{VNF} will each forward the traffic to another pair of \acp{VNF}, which ultimately, forward to a common destination \ac{VNF}.
The third flow switches from VNF3 to VNF4, mixing with the traffic of the first flow at VNF4 and the traffic of the second flow at VNF3.
This example demonstrates support for multiple interfaces, as well as, multiple classifiers and service paths.
Again, the output of \texttt{tcpdump} or \texttt{iftop} within the individual instances can confirm a correct instantiation of forwarding rules.

\begin{figure}
    \centering
    \def\svgwidth{0.8\columnwidth}
    \input{figures/complex-vnffg-example.pdf_tex}
    \caption{Illustration of the multi-interface, multi-path \ac{VNFFG} example.}
    \label{fig:vnffg-complex-example}
\end{figure}


\subsection{Current Status}
In order to evaluate the open issues of \texttt{vim-emu}, first an analysis of the current state of the OpenStack \ac{SFC} implementation is performed with a focus on use cases which are relevant for \ac{OSM}.
This is done by deploying the defined example use cases and collecting the resulting forwarding rules as reference target for the \texttt{vim-emu} implementation.

Early testing revealed that while according to the output of \texttt{openstack sfc port chain list} \ac{OSM} was able to create a forwarding chain, however, actual forwarding was not working.
After lengthy debugging, it became clear that \ac{OSM}, due to a small programming error, always attempted to create forwarding rules using \ac{NSH}~\cite{RFCNSH} as encapsulation mode.
\ac{NSH}, however, required two\footnote{\url{https://review.openstack.org/628149/}}\footnote{\url{https://review.openstack.org/631388/}} patches in order to work with the OpenStack \ac{SFC} implementation.
The latter patch also requires a patch of OpenvSwitch\footnote{\url{https://www.mail-archive.com/ovs-discuss@openvswitch.org/msg05075.html}}.
Without the patches, OpenStack would only partly succeed in registering the required flow rules, leading to a gap in the forwarding table which results in missing flow table entries and thus the drop of the packet.

Thus, Changeset~7124\footnote{\url{https://osm.etsi.org/gerrit/7124/}} was submitted to \ac{OSM} in order to fix the accidental hard-coding to \ac{NSH} encapsulation.
After this fix, the forwarding was possible to observe using \texttt{tcpdump} in VNF2 when deploying the simple example.
In this state, an analysis of the installed rules within the OpenvSwitch was performed.

\Cref{lst:ovs-ofctl-dump-flows} shows the flow rules installed by \ac{SFC} and \Cref{lst:ovs-ofctl-dump-groups} the groups in which OpenStack organizes the forwarding actions for the involved instances.
No detailed analysis will be given here, but the relevant parts will be highlighted as needed in the following sections.

\begin{lstlisting}[caption={OpenFlow table when using OpenStack's \ac{SFC} implementation. The classifier is visible in the matching of the rules at Lines~3--4. Here, the actions are specified in groups (\Cref{lst:ovs-ofctl-dump-groups}). The entries of \texttt{table=5} and \texttt{table=10} are for routing traffic between physical hosts. Since the installation only consists of a single node, \texttt{table=10} undos the changes of \texttt{table=5}.}, label=lst:ovs-ofctl-dump-flows,float]
# ovs-ofctl dump-flows br-int
...
cookie=0xb1faa0d38cd1fd9c, duration=29.297s, table=0, n_packets=0, n_bytes=0, priority=30,tcp,in_port="qvo29ef6007-52", <@\textcolor{red}{nw\_src=10.0.0.10,nw\_dst=10.0.0.12,tp\_dst=80}@> actions=<@\textcolor{red}{group:1}@>
cookie=0xb1faa0d38cd1fd9c, duration=28.196s, table=0, n_packets=0, n_bytes=0, priority=30,tcp,in_port="qvoc2ddbffb-fd", <@\textcolor{red}{nw\_src=10.0.0.10,nw\_dst=10.0.0.12,tp\_dst=80}@> actions=<@\textcolor{red}{group:2}@>
cookie=0xb1faa0d38cd1fd9c, duration=24.930s, table=0, n_packets=0, n_bytes=0, priority=30,tcp,in_port="qvo383859f7-6a", <@\textcolor{red}{nw\_src=10.0.0.10,nw\_dst=10.0.0.12,tp\_dst=80}@> actions=NORMAL
...
cookie=0xb1faa0d38cd1fd9c, duration=29.386s, table=5, n_packets=0, n_bytes=0, priority=0,ip,dl_dst=fa:16:3e:68:e4:a7 actions=<@\textcolor{red}{push\_mpls}@>:0x8847,load:0x1ff->OXM_OF_MPLS_LABEL[], set_mpls_ttl(255),<@\textcolor{red}{mod\_vlan\_vid:16}@>,resubmit(,10)
 cookie=0xb1faa0d38cd1fd9c, duration=28.289s, table=5, n_packets=0, n_bytes=0, priority=0,ip,dl_dst=fa:16:3e:32:b7:53 actions=<@\textcolor{red}{push\_mpls}@>:0x8847,load:0x1fe->OXM_OF_MPLS_LABEL[], set_mpls_ttl(254),<@\textcolor{red}{mod\_vlan\_vid:16}@>,resubmit(,10)
...
cookie=0xb1faa0d38cd1fd9c, duration=28.169s, table=10, n_packets=0, n_bytes=0, priority=1,mpls,dl_vlan=16,dl_dst=fa:16:3e:68:e4:a7, mpls_label=511 actions=<@\textcolor{red}{strip\_vlan}@>,<@\textcolor{red}{pop\_mpls}@>:0x0800,output:"qvoc2ddbffb-fd"
cookie=0xb1faa0d38cd1fd9c, duration=24.881s, table=10, n_packets=0, n_bytes=0, priority=1,mpls,dl_vlan=16,dl_dst=fa:16:3e:32:b7:53, mpls_label=510 actions=<@\textcolor{red}{strip\_vlan}@>,<@\textcolor{red}{pop\_mpls}@>:0x0800,output:"qvo383859f7-6a"
\end{lstlisting}

\begin{lstlisting}[caption={The list of OpenFlow groups. OpenStack organizes each port pair group as an OpenFlow group. This can allow load-balancing, but in this example only a single next hop is defined for each group. The actions modify the MAC address (\texttt{mod\_dl\_dst}) to the next hop in the function chain.}, label=lst:ovs-ofctl-dump-groups,float]
# ovs-ofctl dump-groups br-int
NXST_GROUP_DESC reply (xid=0x2):
<@\textcolor{red}{group\_id=1}@>,type=select,bucket=bucket_id:0, actions=<@\textcolor{red}{mod\_dl\_dst}@>:fa:16:3e:68:e4:a7,resubmit(,5)
<@\textcolor{red}{group\_id=2}@>,type=select,bucket=bucket_id:0, actions=<@\textcolor{red}{mod\_dl\_dst}@>:fa:16:3e:32:b7:53,resubmit(,5)
\end{lstlisting}

Afterwards, an analysis of the available coverage of \texttt{vim-emu} was performed.
The analysis revealed a series of open issues which are summarized in the following.

\subsubsection{1. Shared ingress and egress port not working}
\texttt{vim-emu} expects each \ac{VNF} to have at least two different ports for ingress and egress traffic respectively.
When attempting to use the same port, \texttt{vim-emu} fails to find a server mapping for the second port.

\subsubsection{2. Wrong port pair mapping}
\texttt{vim-emu} links the ingress port as source port and the egress port as target.
However, since the ingress port is the port which receives traffic, while the egress port sends traffic, this does not support use cases where the ingress and egress ports are not shared.

Additionally, as described in \Cref{sec:sfc}, port pairs describe the tuple of the ingress and egress ports of a single service instance.
However, \texttt{vim-emu} implements the port chain creation with the assumption that a port pair maps the link from one service instance to another.
This effectively leads to independent service instances where the ports of the instances are linked to each other, but not in between the services.

\subsubsection{3. No application of classifiers}
Classifiers decide about the path of a flow through a series of service functions.
Next to potential filtering upon source and target IP or port ranges, most importantly, the classifier also contains the logical source port, which identifies the port on which the traffic is classified.
Since \texttt{vim-emu} does not consider classifiers while instantiating service chains, this source port information is not available, effectively disconnecting the forwarding graph from the source, disallowing any traffic to enter the graph.

\subsubsection{4. Wrong application of VLAN tagging}
VLAN tagging, MPLS~\cite{RFCMPLS} or \ac{NSH} allow to describe arbitrary virtual network topologies on top of a fixed underlying topology.
OpenStack's \ac{SFC} implementation, for example, uses these techniques for encapsulating traffic across different physical machines within the cloud infrastructure.
However, before forwarding a packet to the receiving service, the virtual switch removes the additional routing information, making the mechanism transparent to the actual \acp{VNF}.
\texttt{vim-emu} also applies VLAN tagging on the interfaces which are part of a service chain, but neither removes the packet wrapping before forwarding the packet to the service, nor actually performs routing decisions based on these labels.
Additionally, the tagging is performed unconditionally on all traffic traveling through the port, disallowing the use of multiple classifiers targeting a single port.

\subsubsection{5. No update of MAC address when changing course of packet}
Even with the above issues resolved, the forwarding rules of \texttt{vim-emu} do not change the destination MAC address of a packet.
While the packets still get forwarded to the correct interface, that interface will see a MAC address which does not match its own and thus rejects the packet.


\subsection{Implementation}
\label{sec:vnffg-impl}
After the identification of open issues, this section describes the way each of the individual problems was solved.
In each subsection a problem is explained in greater detail, solution candidates are evaluated and a solution is presented and tested with the examples described at the start of this section.

\subsubsection{1. Shared ingress and egress port not working}
Support for different ingress and egress port landed only shortly after Release~FIVE of \ac{OSM}~\cite{osm-commit-different-ingress-egress}.
However, the \ac{SFC} implementation in \texttt{vim-emu} in this regards suffers from a small logic flaw which seems to be an oversight.
\Cref{lst:vim-emu-identical-port-bug} shows the code which attempts to return the servers for the ingress and egress ports.
Since the check for the egress port is in an \texttt{elif} branch, it will only be executed if the preceding \texttt{if} clause failed to match.
Therefore, the \texttt{elif} branch is not evaluated if the ingress and egress port are identical.
This leads to the code being unable to find the server instance of the egress port, which blocks the creation of the port chain.

\begin{lstlisting}[caption={The errorneous code which disallowed the use of identical ingress and egress ports~\cite{osm-identical-port-bug}.}, label=lst:vim-emu-identical-port-bug,language=python,float,firstnumber=74]
if port_pair.ingress.name in server.port_names:
    server_ingress = server
elif port_pair.egress.name in server.port_names:
    server_egress = server
\end{lstlisting}

\paragraph{Solution Idea:}
In this case the solution is straight forward.
The \texttt{elif} needs to be replaced with a \texttt{if} in order to allow the execution of both code branches in case the condition matches.

\paragraph{Implementation and Test:}
The fix was submitted as Changeset~7157\footnote{\url{https://osm.etsi.org/gerrit/7157/}}.
However, testing with the simple use case revealed a series of other small, related issues.

First, \texttt{vim-emu} disallows multiple ports with the same name.
Since the port name is specified in the \ac{VNFD} and the defined use case creates multiple instances of that \ac{VNFD}, this inherently leads to multiple port instances with the same name.
The prevention of multiple ports with the same name made the finding of ports slightly easier, since that code could rely on at most a single port existing.
Thus, Changeset~7151\footnote{\url{https://osm.etsi.org/gerrit/7151/}} removes the duplicate name check from the creation, but adds the check to the logic which searches for a specific port.
Since \ac{OSM} always uses IDs to reference ports, this keeps the code backwards-compatible by still throwing an error if no unique port can be matched.

Secondly, OpenStack allows to filter the port list by a \texttt{device\_id}.
This enables to list the ports assigned to a specific \ac{VM} instance, a method used by \ac{OSM} in order to verify the correct instantiation of the port.
This additional filter method is added with Changeset~7152\footnote{\url{https://osm.etsi.org/gerrit/7152/}}.
A similar issue existed for the port chain \ac{API}.
Here, \ac{OSM} attempts to filter the list by ID in order to verify the status of the chain. Changeset~\footnote{\url{https://osm.etsi.org/gerrit/7154/}} resolves this issue.

Next, \ac{OSM} requires the \texttt{service\_function\_parameters} field to exist on the response of the port pair \ac{API}.
The field is never set by \ac{OSM}, but the conversion logic of the response still attempts to access the field~\cite{osm-service-function-parameters-access}.
Therefore, returning an empty array is added in Changeset~7153\footnote{\url{https://osm.etsi.org/gerrit/7153/}}.

Finally, the expectation of name usage in order to specify ports led to another issue within \Cref{lst:vim-emu-identical-port-bug}.
When being invoked by \ac{OSM}, the \texttt{server.port\_names} array will contain ids instead of actual names.
Thus, Changeset~7156\footnote{\url{https://osm.etsi.org/gerrit/7156/}} extends the \texttt{if} branches with an \texttt{or} check for the \texttt{id} field being part of \texttt{server.port\_names}.

After the fixes, it is possible to observe rules being installed by \texttt{vim-emu} by executing \texttt{ovs-ofctl dump-flows dc1.s1}.
But the remaining issues prevent meaningful tests at this stage.


\subsubsection{2. Wrong port pair mapping}
Port pairs group the ingress and egress ports of a service instance together.
The ingress port is the port at which the service expects to receive the re-routed traffic.
Similarly, the egress port marks the port at which the service is expected to output the packets that are relevant to the flow.

When generating forwarding rules from a port chain consisting of multiple port pairs, this means that the traffic of an egress port needs to be forwarded to the ingress port of next port pair in the chain.

\texttt{vim-emu} here mixes up the ingress and egress ports, as well as, only creates forwarding rules which forwarded between the ports within a single port pair.

\paragraph{Solution Idea:}
First, the direction of the port mapping has to be fixed.
From the switch's point of view, the egress port of a service has to be an input and the service's ingress port has to be an output.

Additionally, \texttt{vim-emu} needs to link port pairs together, not the individual ports within a single port pair.

\paragraph{Implementation and Test:}
\Cref{lst:port-chain-iteration} shows the code~\cite{vim-emu-port-chain-iteration} which iterates over the port pairs in order to install the required chain parts.
Here, fixing the linkage between the individual service instances mainly requires to change the iteration from iterating over the port pairs to iterating over the tuples of \texttt{(ingress, egress)} ports which should be linked according to their occurrence within the port chain.

\begin{lstlisting}[caption={The code responsible for installing a port chain after fixing the \texttt{elif} logic~\cite{vim-emu-port-chain-iteration}.}, label=lst:port-chain-iteration,float,firstnumber=65]
for group_id in self.port_pair_groups:
    port_pair_group = compute.find_port_pair_group_by_name_or_id(
        group_id)
    for port_pair_id in port_pair_group.port_pairs:
        port_pair = compute.find_port_pair_by_name_or_id (port_pair_id)

        server_ingress = None
        server_egress = None
        for server in compute.computeUnits.values():
            if port_pair.ingress.name in server.port_names or port_pair.ingress.id in server.port_names:
                server_ingress = server
            if port_pair.egress.name in server.port_names or port_pair.egress.id in server.port_names:
                server_egress = server

        if not server_ingress:
            raise RuntimeError("Neutron SFC: ingress port %s not connected to any server." %
                               port_pair.ingress.name)
        if not server_egress:
            raise RuntimeError("Neutron SFC: egress port %s not connected to any server." %
                               port_pair.egress.name)

        compute.dc.net.setChain(
            server_ingress.name, server_egress.name,
            port_pair.ingress.intf_name, port_pair.egress.intf_name,
            cmd="add-flow", cookie=self.cookie, priority=10, bidirectional=False,
            monitor=False
        )
\end{lstlisting}

This could be done by still iterating over the port pairs, but referring to the next port pair within the chain in order to acquire its ingress port.
However, doing such look-aheads within the loop would create additional complexity compared to the existing \texttt{(ingress, egress)} iteration.

Therefore, an easier solution in regards to code complexity would be to form the correct \texttt{(ingress, egress)} tuples before the loop.
If, for simplicity, the individual ingress and egress ports for each port pair are assumed to be names of interfaces:

\begin{lstlisting}[numbers=none]
>>> ingress_ports = ['i1', 'i2', 'i3', 'i4']
>>> egress_ports = ['e1', 'e2', 'e3', 'e4']
\end{lstlisting}

the iteration over the required port mappings would be much easier if it is possible to iterate over a list which contains the mappings \texttt{e1->i2}, \texttt{e2->i3} and \texttt{e3->i4}.
It is possible to generate such a list of tuples by zipping\footnote{\url{https://docs.python.org/3/library/functions.html\#zip}} the list of egress ports with the ingress ports, while omitting the first ingress port:

\label{zip-example}

\begin{lstlisting}[numbers=none]
>>> zip(egress_ports, ingress_ports[1:])
[('e1', 'i2'), ('e2', 'i3'), ('e3', 'i4')]
\end{lstlisting}

The first ingress port is omitted, because it marks the entry point into the port chain.
Entering the port chain is done by matching traffic against classifiers, which will be implemented in a later step and thus is being ignored here.

In the real implementation this is done with actual port objects, but otherwise the concept is the same.

In order to fix the direction of the port forwarding rule, the arguments to the \texttt{setChain} function call have to be flipped.
The function expects the parameters \texttt{vnf\_src\_name}, \texttt{vnf\_dst\_name}, \texttt{vnf\_src\_interface}, \texttt{vnf\_dst\_interface}, while being invoked with the ingress server name, the egress server name, the ingress interface name and the egress interface name.
Here, the order of first and second parameter, as well as the third and fourth parameter, has to be switched.

Together, these changes are submitted as Changeset~7227\footnote{\label{c7227} \url{https://osm.etsi.org/gerrit/7227/}}, after which the forwarding to the correct service functions can be observed using the simple example and running \texttt{tcpdump} within VNF3 while generating some kind of traffic from VNF2 which usually would not be routed to VNF3.


\subsubsection{3. No application of classifiers}
After the basic forwarding is in place, there is still no way for packets to actually enter the chain.
Determining which packets should flow along which port chain is done using classifiers.
In \ac{OSM} a classifier consists of information about a source port within a \ac{VNF} instance, a list of matches and a reference to the service path~\cite{osm-nsd-file}.
The matches can filter traffic based upon the IP protocol type (ICMP, TCP, UDP, ...), the source or target IP address, as well as the source or destination TCP or UDP port numbers.
In \texttt{vim-emu} these classifiers are completely ignored, disconnecting the installed forwarding graph from the traffic source.

\paragraph{Solution Idea:}
In order to use \texttt{vim-emu} as prototyping platform for developing \acp{VNFFG}, depending on the complexity of the scenario to prototype, a full support of classifiers might not be required.
For example, in order to test the simple use case with a single classifier and forwarding path, it would be sufficient to install a forwarding rule which forwards all the traffic, regardless of source or destination.
Even the first and second flow of the complex scenario (\Cref{fig:vnffg-complex-example}) do not require forwarding decisions which depend on the packet content.
Only when adding the third flow the source port itself is no longer sufficient for identifying the next hop of a packet.
In that case the traffic coming from VNF3 may need to be forwarded to either VNF4 or VNF5, depending on the destination specified in the IP header of the packet.

Therefore, a simple solution which only classifies traffic based on the source port can be implemented.
Later, a more sophisticated classification, which actually considers the match attributes can be added as an extension.

\paragraph{Implementation and Test:}
Implementing the basic solution becomes relatively simple due to the decision to use an intermediate list which already consists of all the required port mappings in Changeset~7227\cref{c7227}.
Without needing to further increase the complexity of the existing loop logic only the list of egress to ingress port mappings requires an update.
Here, generating a list of tuples which pair the \texttt{logical\_source\_port} of the classifiers with the previously ignored first ingress port and prepending that list to the existing port mappings is enough.
This is done in Changeset~7228\footnote{\url{https://osm.etsi.org/gerrit/7228/}}.

However, in order to extend the solution with support for more complex flow classifiers, simply applying the classifier at the start of the port chain will not work.
As illustrated in \Cref{fig:classifier-extension-problem}, installing flow rules which unconditionally forward the traffic between the nodes based on the egress port leads to a conflict when a forwarding decision is needed in order to forward traffic from VNF3 to VNF4 or VNF5.
Thus, additional forwarding rules will be required.

\begin{figure}
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{figures/classifier-extension-problem.pdf_tex}
    \caption{Illustration of the forwarding problem when the forwarding decision is solely based on the output port. While VNF3 is supposed to forward traffic to VNF5 for the second flow, the third flow mandates a forward to VNF4.}
    \label{fig:classifier-extension-problem}
\end{figure}

For routing traffic along a specific path multiple standard techniques exist. 802.1Q~\cite{vlan802_1Q}, MPLS~\cite{RFCMPLS}, NSH~\cite{RFCNSH} or alike solve this issue by wrapping packets with additional control headers.
These headers can include identifiers which are then matchable by flowtables in order to allow flexible routing along specific paths within the network.
Attempting to pair these techniques with \ac{SFC}, however, leads to some issues.
First, a \ac{VNF} would need to be able to deal with the wrapped packets of the individual switching protocols.
It would need to be able to parse the packet, manipulate it and forward it while preserving the flow identifier.
Fortunately, the Linux kernel has support for these protocols~\cite{kernel-net-subtree} which turns the problem into a configuration issue mostly.

More importantly, passing packets with flow identifiers to a \ac{VNF} increases the coupling of the \ac{VNF} and the switching technique.
While the idea of \ac{SFC} is that functions can be chained without necessarily knowing about each other, forcing service functions to preserve and generate flow identifiers in their output packets blurs the task of the switching logic and the actual logic of the network function.
Especially for network functions which generate traffic without a source, it would be necessary to inform the \ac{VNF} about the type of switching and label to use.
Due to this, the OpenStack \ac{SFC} logic only uses these kind of traffic encapsulation methods for routing the traffic to the correct physical machine, but then removes the encapsulation before forwarding packets to the actual \acp{VM}.

All in all, this means that the previous approach of only applying the classifiers when entering the forwarding chain is not applicable for more complex scenarios.
Instead, the classifier also needs to be part of the forwarding decision of the individual hops within the port chain.
Thus, instead of prepending all classifiers to the start of a port chain, a forwarding chain for each classifier needs to instantiated.
This is done in Changeset~7242\footnote{\label{c7242} \url{https://osm.etsi.org/gerrit/7242/}} by wrapping the existing iteration over the port chain elements with an iteration over the classifiers.


Additionally, the changeset also implements the match rules for separating the individual forwarding chains.
For this, the \texttt{FlowClassifier} datastructure of \texttt{vim-emu} needs to be translated into an OpenFlow compatible match string~\cite{openflow-match-fields}.
Here, it has to be considered that OpenFlow rules can only match on fields in higher level packet layers if the required underlying layers are narrowed first.
In other words, when defining a rule which matches on a specific TCP target port, first the packet needs to be matched as IP packet, followed by identifying the IP packet as containing a TCP payload.
This means that the rule \textit{source:~10.0.0.10}, \textit{target:~10.0.0.12} and \textit{target~port:~80} translates to:

\begin{lstlisting}[numbers=none]
dl_type=2048,nw_proto=6,nw_src=10.0.0.10/32, nw_dst=10.0.0.12/32,tp_dst=80
\end{lstlisting}

Here, \texttt{dl\_type=2048} matches an IPv4 packet, allowing access to the header fields of the IP packet (\texttt{nw\_proto}, \texttt{nw\_src} and \texttt{nw\_dst}).
The match \texttt{nw\_proto=6} further narrows the match to IPv4 packets with the protocol being set to TCP.
Again, this allows checking for values in the TCP layer, allowing to restrict the destination port to 80.

Since \ac{OSM} in the current implementation only allows to define classifiers based on IPv4 packets, the changeset only implements matches which match on a source or destination IP for either ICMP or TCP packets, along with an optional restriction on a target port.
However, the code should be easily extendable in order to support more sophisticated classifiers in the future.


\subsubsection{4. Wrong application of VLAN tagging}
\texttt{vim-emu} configures each outgoing port, which is part of a forwarding chain, to tag the traffic with a VLAN identifier.
This kind of tagging is done by directly tagging the port using \texttt{ovs-vsctl}, which makes OpenvSwitch apply the VLAN identifier regardless of matches in the flow table rules.
The applied VLAN tag also was never removed before forwarding the packet to a service instance.
This leads to the problems explained in the previous section, namely requiring the \ac{VNF} to deal with the VLAN identifier, breaking compatibility with OpenStack's \ac{SFC} \ac{API}.

\paragraph{Solution Idea:}
Since after Changeset~7242\cref{c7242} traffic is routed to the correct machine without the need of VLAN tagging, it would be possible to simply remove the VLAN tagging.
However, \texttt{vim-emu} also serves as emulation platform for the SONATA project\footnote{\url{http://sonata-nfv.eu}}, which uses this VLAN tagging mechanism.

Luckily, the tagging logic can be turned off using an optional parameter when calling the internal helper function which installs the flow rules.

\paragraph{Implementation and Test:}
Thus, Changeset~7237\footnote{\url{https://osm.etsi.org/gerrit/7237/}} is able to disable the VLAN tagging for the OpenStack like port chain emulation, leaving the existing logic as it is.


\subsubsection{5. No update of MAC address when changing course of packet}
After the previously described fixes, the packets should now arrive at the correct service in the correct format.
However, this service is likely going to reject the packet that it is receiving.
The reason for this is that it does not see its own MAC address within the received packet.
Since \ac{SFC} is designed to be transparent for the involved parties, each \ac{VNF} will always send its traffic to the final destination (or leave the destination of modified packets intact).
Thus, packets will only be sent to the MAC address which would also be the target in a non-\ac{SFC} scenario.
If a packet now is rerouted to a different destination, that destination will have a different MAC address, leading to the packet drop described above.

\paragraph{Solution Idea:}
Taking a look at \Cref{lst:ovs-ofctl-dump-groups} reveals that OpenStack's \ac{SFC} mechanism installs specific actions which change the MAC address of a packet before forwarding it to a machine.
This is done using the \texttt{mod\_dl\_dst} action within the \texttt{actions} field of the flow group.
While \texttt{vim-emu} does not use flow groups for generating the forwarding actions, the same principle can be applied by directly writing the relevant action into the \texttt{actions} field of the flow table.

\paragraph{Implementation and Test:}
\texttt{vim-emu} is using the \texttt{ryu-manager}\footnote{\url{https://ryu.readthedocs.io/en/latest/index.html}} as controller for the OpenvSwitch.
RYU comes with a \ac{REST} \ac{API} which is consumed by \texttt{vim-emu} in order to install flow rules.
For this, the \ac{API} is called with \texttt{match} and \texttt{actions} fields which allow to define OpenFlow matches or actions.
Thus, for adding an action which changes the MAC address of a packet, the action needs to be added to the existing \texttt{actions} of the \ac{API} call.
For OpenFlow~1.2 or later, this action to change field values is called \texttt{SET\_FIELD}\footnote{\url{https://ryu.readthedocs.io/en/latest/app/ofctl_rest.html\#example-of-set-field-action}} and is called with the name of the packet header field to override, as well as the new value for the field.
The field name which represents the destination MAC address is \texttt{eth\_dst} and the value of the MAC address is already known to \texttt{vim-emu}.

Thus, Changeset~7238\footnote{\url{https://osm.etsi.org/gerrit/7238/}} adds an optional \texttt{mod\_dl\_dst} parameter to the internal \texttt{\_set\_flow\_entry\_ryu\_rest} function and sets the parameter when installing \ac{SFC} rules.
After this, the simple example can be used to demonstrate a fully working forwarding chain.
Since Docker containers enable \texttt{net.ipv4.ip\_forward} by default, VNF2 receives the packets targeted at its MAC address and automatically forwards those which do not target its own IP address.
Then again, the forwarded packets of VNF2 are matched against the classifier and their target MAC address is adjusted, allowing VNF3 to handle them.

The complex example, however, fails to deploy with error messages which indicate that a port chain is being created without any hops in the chain.
Only the last chain from the descriptor file is reported as successfully created.
Analysis of the \ac{RO} source code revealed that an array tracking the individual hops of the port chains is overridden during the iteration over the \texttt{rsp} elements of the descriptor.
Therefore, incomplete chains are written to the database of \ac{RO}, resulting in incomplete data for the deployment step.
A fix for the problem was submitted as Changeset~7353\footnote{\url{https://osm.etsi.org/gerrit/7353}}.
Again, the bug indicates that \acp{VNFFG} are largely untested on a larger scale.
The developed solution simplifies this process and therefore helps to detect these kind of issues earlier.

