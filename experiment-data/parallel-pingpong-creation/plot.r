library(tidyverse)
library(gmodels)

plot_width <- 6
plot_height <- 3.5


d = read.csv("parallel-pingpong-creation/5/merged.csv") %>%
  rename(`NS deploy`=ns_start, `NS deletion`=ns_delete) %>%
  gather(Var, Value, -n) %>%
  group_by(n, Var) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )

deploy_delete <- d
deploy_delete$Var <- factor(deploy_delete$Var, levels=c("NS deploy", "NS deletion"))
width <- 0.9
position=position_dodge(width=width)
ggplot(filter(deploy_delete, !is.na(Var)), aes(x=n, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name="Number of parallelly deployed NSs", breaks=seq(0, 30, 5)) + 
  scale_y_continuous(name="Mean duration [s]") +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_ping_pong_parallel_deploy_and_delete.eps', width=plot_width, height=plot_height)

failed <- d
failed$Var <- factor(failed$Var, levels=c("num_failed"))
width <- 0.9
position=position_dodge(width=width)
ggplot(filter(failed, !is.na(Var)), aes(x=n, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name="Number of parallelly deployed NSs", breaks=seq(0, 30, 5)) + 
  scale_y_continuous(name="Mean number of failed deployments") +
  labs(fill="") +
  theme_bw() +
  theme(legend.position = "none")
ggsave('../thesis/figures/plot_ping_pong_parallel_num_failed.eps', width=plot_width, height=plot_height)
