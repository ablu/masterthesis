Proposal to (optionally) launch OSM from vim-emu
================================================

Introduction
------------
As part of my masterthesis I am looking for feedback for this proposal. If the OSM community is interested in this feature I am willing to implement it as part of my thesis (and contribute it to vim-emu).

Motivation
----------
OSM in combination with vim-emu is a promising platform for prototyping, however, currently efficient prototyping is limited by repetitively needing to package, board, launch and unboard VNFs and NSs. With relatively simply changes it should be possible to greatly enhance the prototyping process by minimizing manual steps as well as offering new testing scenarions for OSM itself.

Idea
----
Extend the vim-emu API with helpers to launch the OSM services.
This could vary in terms of simplicity and flexibility, but an example which should demonstrate the idea could look like:

**Chosen API naming is for demonstrating the concept only!**

::

    with PreConfiguredOSM() as osm:
        osm.start()
        osm.onboard_vnfd('vnfd/')
        nsd_id = osm.onboard_nsd('nsd/')
        osm.ns_create('ns_name', nsd_id)
        osm.ns_wait_until_all_in_status('running')
        # confirm that the NS works...
        osm.stop()

This script would then automatically launch an OSM instance (it should be enough to launch some variant of `devops/installers/docker/docker-compose.yaml`_), package the VNF and NS, onboard them, register the (fake-)VIM and instantiate the NS.

Different levels of the API can allow more detailed control over the topology:

mid-level (VIM is handled by script manually):

::

    net = DCNetwork()
    dc1 = net.addDatacenter("dc1")
    api1 = OpenstackApiEndpoint("0.0.0.0", 6001)
    api1.connect_datacenter(dc1)
    api1.connect_dc_network(net)
    api2 = OpenstackApiEndpoint("0.0.0.0", 6002)
    api2.connect_datacenter(dc2)
    api2.connect_dc_network(net)
    net.addLink(dc1, dc2, cls=TCLink, delay="50ms")

    osm = OSM(net)
    api1.start()
    api2.start()
    osm.start()
    net.start()

    vim1_id = osm.register_emulated_vim('vim1', api1)
    vim2_id = osm.register_emulated_vim('vim2', api2)

    osm.onboard_vnfd('vnfd/')
    nsd_id = osm.onboard_nsd('nsd/')
    ns1_id = osm.ns_create('ns_name', nsd_id, vim1_id)
    ns2_id = osm.ns_create('ns_name', nsd_id, vim2_id)
    osm.ns_wait_until_all_in_status('running')
    # confirm that the NSs work...
    osm.ns_delete(ns1_id)
    osm.ns_delete(ns2_id)
    osm.ns_wait_until_all_in_status('terminated')
    osm.stop()
    api1.stop()
    api2.stop()
    net.stop()

low-level:

::

    net = DCNetwork()
    dc1 = net.addDatacenter("dc1")
    api1 = OpenstackApiEndpoint("0.0.0.0", 6001)
    api1.connect_datacenter(dc1)
    api1.connect_dc_network(net)

    s1 = net.addSwitch('s1')

    # definition of Kafka, Mongo, ...
    nbi = NBI(net, '10.0.0.101', mongo_ip, kafka_ip)
    # ...
    net.addLink(kafka, s1)
    net.addLink(nbi, s1)
    # ...
    kafka.start()
    nbi.start()
    # ...
    api1.start()
    net.start()

    vim_id = nbi.register_emulated_vim('vim1', api1)
    nbi.onboard_vnfd('vnfd/')
    nsd_id = nbi.onboard_nsd('nsd/')
    nbi.ns_create('ns_name', nsd_id, vim_id)
    nbi.ns_wait_until_all_in_status('running')
    # confirm that the NS works...
    api1.stop()
    net.stop()

.. _devops/installers/docker/docker-compose.yaml: https://osm.etsi.org/gitweb/?p=osm/devops.git;a=blob;f=installers/docker/docker-compose.yaml;h=b37e38105e50dd75b7f29edc2960c38d9e89fe50;hb=HEAD

Benefits
--------
The proposal can cover a series of interesting use cases:

- Simpler, reproducible prototyping
- Simple version changes
- Allows testing use cases with multiple OSM instances (some effort may be necessary in order to allow conflict-free, parallel launching)
- Support for defining network constraints (link delay, etc) come "for free" from the underlying Mininet.
- Underlying Mininet may allow simple emulation of distributed OSM installations (but will require more flexible API in order to start individual OSM services in different, emulated datacenters.
- No existing use cases are limited, since it is just an additional (optional) API