\section{OPNFV}
\label{sec:opnfv}
\ac{OPNFV} is a project under maintenance of the \ac{LFN} which aims to create collaborative tools for creating an open source foundation for \ac{VNF} solutions.

The organization is an umbrella project which contributed to a series of subprojects~\cite{opnfv-software}.
These subprojects mostly include management tools for simplifying the setup process of private cloud infrastructure, but also a series of testing tools\footnote{\url{https://wiki.opnfv.org/display/testing/TestPerf}}:

\begin{description}
    \item[Functest] Provides testing of base system (\ac{VIM} and \ac{NFVI} layers) functionality.
    The project executes a series of pre-defined test-cases, ranging from simple \ac{API} availability checks to spawning a set of \ac{VNF} instances and testing their interaction.
    The tool is tailored towards evaluating and monitoring the functionality of production-ready deployments of \ac{NFVI}.
    \item[QTIP] Advertised as ``Benchmarking as a Service'', this tool is a higher level utility for implementing benchmarks.
    It can aggregate the results of lower-level performance testing into more easily interpretable indicators.
    \item[Bottlenecks] A project to provide tooling for System Limitation Testing.
    The project provides test cases for testing limits of resources like storage, bandwith or scaling capability.
    \item[Yardstick] A test framework for performing \ac{NFVI} verification and compliance testing.
    Operators can, for example, define constraints like an upper bound for the time of a ping between two instances on the infrastructure.
    The test scenarios can be described in YAML and can be aggregated into test suites for automated execution.
    \item[StorPerf] A tooling collection for measuring performance of block and object storage within the \ac{NFVI}.
    \item[VSperf] A project which provides a data-plane performance measurement tool.
\end{description}

While this toolset covers a wide area, all tools have in common that they test existing infrastructure, sometimes by utilizing pre-defined \acp{VNF}, but not by using user-defined (potentially in development) \acp{VNF}.
An extension of the tool landscape in order to verify individual \acp{VNF} seem to be in progress, but seems to be targeted towards providing certification of \acp{VNF}\footnote{\url{https://www.opnfv.org/verified}}.
Thus, again, the tooling is targeted to work against existing infrastructure, making it hard to utilize in development scenarios which are covered by the solution provided by this thesis.

\section{ONAP OTF}
The Open Test Framework~\cite{onap-otf} is a work-in-progress project for the ONAP platform.
So far the project composed a summary of the ONAP testing situation and came to the conclusion that, while some aspects like \ac{NFVI} and \ac{MANO} testing can be performed using \ac{OPNFV}~(\Cref{sec:opnfv}) or the ONAP Operations Manager, the aspects of testing \acp{VNF} or \acp{NS} are not well covered.

For \ac{VNF} testing, ONAP offers the project called VNFSDK\footnote{\url{https://onap.readthedocs.io/en/amsterdam/submodules/vnfsdk/model.git/docs/}}.
However, the relevant subproject \ac{VTP}\footnote{\url{https://wiki.onap.org/pages/viewpage.action?pageId=43386304}} is still in the planning phase.
The current slides on the topic, however, reveal a focus more based on verification and certification of readily available \ac{VNF} packages rather than \acp{VNF} being in development. But, since the platform is targeted to be deployable as a Docker container, it might be suitable to be executed on development hardware, depending on the availability of the runtime dependencies of the platform.

\section{DOCKEMU}
DOCKEMU~\cite{dockemu}, being similar to Containernet, allows to launch Docker containers in an isolated IP networking namespace. Essentially, it consists of a simple bash script\footnote{\url{https://github.com/jmarcos-cano/dockemu/blob/master/dockemu}} and thus is a very simple starting point for emulating networks.

However, it is limited to starting a fixed amount of containers, all being based on the same image.
Thus, Containernet provides all the features of DOCKEMU with more flexibility, but also more complexity.

%\section{EsCAPE}

\section{NIEP}
NIEP~\cite{tavares2018niep} is a \ac{NFV} Infrastructure Emulation Platform which is utilizing Mininet~\cite{mininet} in combination with Click-on-OSv~\cite{da2017click}.
The platform launches minimized \ac{VM} instances, whose topology is managed using Mininet.

Since the platform works with industry standard \ac{VM} images, prototyped solutions can be also deployed on production-ready orchestrators without further changes.
However, even though OSv\footnote{\url{http://osv.io/}} can reduce the resource requirements in comparison to traditional \acp{VM}, container technology will still out-perform any \ac{VM} based solution in terms of memory usage~\cite{Chae2017}, allowing the emulation of significantly more instances on the same hardware.

Being designed as prototyping platform, NIEP does not integrate with production-ready orchestration software, disallowing integration testing of the full stack.

\section{EmuStack}
EmuStack~\cite{emustack} provides a network emulation platform by combining OpenStack with Linux \ac{TC}.
The authors describe a system where they deploy OpenStack in combination with Docker containers and allow configuration of links via \ac{TC} and network namespaces.
The system still requires a rather resource heavy OpenStack installation, but benefits from the more lightweight isolation provided by the Docker hypervisor driver.
Unfortunately, no source code was made available online for further analysis.


\section{Further research work}
Next to the more practical oriented work, a series of considerations and informational reports on the topic of \ac{VNF} testing exists.
IETF BMWG published a memo~\cite{morton2017considerations} which extends existing industry standards for benchmarking to \ac{VNF} specific requirements.
The memo suggests new benchmarking metrics such as the time to deploy or migrate \acp{VNF}, which certainly are also interesting to measure for emulated environments, in order to evaluate the performance of the emulation. The metrics could also allow to compare a real \ac{MANO} stack with an emulated environment in order to transfer insights from emulation to production or vice versa.

Further standardization of \ac{VNF} testing is provided by the ETSI GS NFV-TST 001 standard~\cite{etsi-nfv-test-001}. The standard suggests methods to validate \ac{VNF} environments before deployment.
This covers validation of the \ac{NFVI}, but also provides test cases for lifecycle management or data plane testing of \acp{VNF} or entire \acp{NS}.

\section{Summary}
The analysis of the related work reveals that while \acp{VNF} themselves received a lot of attention in both industry and research, the field of emulating or testing \acp{VNF} only yields few results.
While OPNFV focuses on the infrastructure layer and NIEP on the actual virtual machines, only EmuStack attempts to provide a full-stack emulation platform at the cost of depending on large parts of the resource-heavy OpenStack.
Unfortunately, the project's source code was not published, disallowing further analysis.

Otherwise, the existing research and industry seems to focus on providing tools or metrics for certification of \acp{VNF}.
While this may help to identify compatibility issues of existing \acp{VNF}, this does not support the complex development process of a \ac{VNF}.

The project which comes closest to \texttt{vim-emu} seems to be NIEP.
However, the lacking integration with existing orchestrators leaves \texttt{vim-emu} as the most advanced emulation platform and thus most promising candidate as base for this work.
The extensions to \texttt{vim-emu} which are developed as part of this work also solve many of the compatibility issues which an OpenStack based project like EmuStack may not suffer from, while also providing enhanced prototyping capabilities and most likely better performance.
Unlike the mentioned projects, this work achieves a full emulation of the whole \ac{NFV} stack.
