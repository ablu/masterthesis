#!/bin/bash
set -xe

osm ns-list | grep -oE "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"|xargs -rn 1 osm ns-delete

osm nsd-list | grep -oE "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"|xargs -rn 1 osm nsd-delete
osm vnfd-list | grep -oE "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"|xargs -rn 1 osm vnfd-delete

osm nsd-list
osm vnfd-list
