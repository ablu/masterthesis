# My master thesis

## latest links to documents:

 * [Thesis](https://gitlab.com/ablu/masterthesis/-/jobs/artifacts/master/raw/build/Master%20Thesis%20Erik%20Schilling.pdf?job=build)
 * [Presentation Proposal](https://gitlab.com/ablu/masterthesis/-/jobs/artifacts/master/raw/build/Presentation%20Proposal%20Master%20Thesis%20Erik%20Schilling.pdf?job=build)
 * [Proposal](https://gitlab.com/ablu/masterthesis/-/jobs/artifacts/master/raw/build/Proposal%20Master%20Thesis%20Erik%20Schilling.pdf?job=build)
 * [Proposal OSM Prototyping](https://gitlab.com/ablu/masterthesis/-/jobs/artifacts/master/raw/build/Presentation%20Proposal%20Hackfest.pdf?job=build)
 * [Presentation Defense](https://gitlab.com/ablu/masterthesis/-/jobs/artifacts/master/raw/build/Presentation%20Defense%20Master%20Thesis%20Erik%20Schilling.pdf?job=build)
