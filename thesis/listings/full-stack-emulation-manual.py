import subprocess
import time
from io import BytesIO

import docker

from mininet.net import Containernet
from mininet.log import info, setLogLevel, debug

setLogLevel('info')


def wrap_debian_like(image):
    dcli = docker.from_env().api
    dockerfile = '''
    FROM %s
    RUN apt update && apt install -y net-tools iputils-ping iproute
    ''' % image
    f = BytesIO(dockerfile.encode('utf-8'))
    wrapper_name = '%s_%s' % (image, 'containernet_compatible')
    build_stream = dcli.build(fileobj=f, tag=wrapper_name)
    build_result = [line for line in build_stream]
    debug('Docker build result:' + '\n'.join(build_result) + '\n')
    return wrapper_name


def wait_until(cmd):
    debug('waiting for %s\n' % cmd)
    while subprocess.call(cmd, shell=True) != 0:
        time.sleep(1)


net = Containernet()
net.addController('c0')

d1 = net.addDocker('d1', ip='10.0.0.100', dimage="ubuntu:trusty")
zookeeper = net.addDocker('zookeeper', ip='10.0.0.101', dimage='wurstmeister/zookeeper')
kafka = net.addDocker(
    'kafka', ip='10.0.0.102', dimage='wurstmeister/kafka',
    environment={'KAFKA_ADVERTISED_HOST_NAME': '172.17.0.4',
                    'KAFKA_ADVERTISED_PORT': '9092',
                    'KAFKA_ZOOKEEPER_CONNECT': '10.0.0.101:2181',
                    'KAFKA_CREATE_TOPICS': 'admin:1:1,ns:1:1,vim_account:1:1, wim_account:1:1,sdn:1:1,nsi:1:1'})
mongo = net.addDocker('mongo', ip='10.0.0.103', dimage=wrap_debian_like('mongo'))
nbi = net.addDocker(
    'nbi', ip='10.0.0.104', dimage=wrap_debian_like( 'opensourcemano/nbi:releasefive-daily'),
    environment={'OSMNBI_DATABASE_URI': 'mongodb://10.0.0.103:27017',
                    'OSMNBI_MESSAGE_HOST': '10.0.0.102'})
ro_db = net.addDocker(
    'ro-db', ip='10.0.0.105', dimage=wrap_debian_like("mysql:5"),
    environment={'MYSQL_ROOT_PASSWORD': 'TEST'})
ro = net.addDocker(
    'ro', ip='10.0.0.106', dimage=wrap_debian_like( 'opensourcemano/ro:releasefive-daily'),
    environment={'RO_DB_HOST': '10.0.0.105', 'RO_DB_ROOT_PASSWORD': 'TEST'})
lcm = net.addDocker(
        'lcm', dimage=wrap_debian_like( 'opensourcemano/lcm:releasefive-daily'),
        environment={
            'OSMLCM_RO_HOST': '10.0.0.106',
            'OSMLCM_VCA_HOST': '172.17.0.1',
            'OSMLCM_DATABASE_URI': 'mongodb://10.0.0.103:27017',
            'OSMLCM_MESSAGE_HOST': '10.0.0.102',
        })

s1 = net.addSwitch('s1')

net.addLink(d1, s1)
net.addLink(zookeeper, s1)
net.addLink(kafka, s1)
net.addLink(mongo, s1)
net.addLink(nbi, s1)
net.addLink(ro, s1)
net.addLink(ro_db, s1)
net.addLink(lcm, s1)

net.start()
zookeeper.start()
kafka.start()
wait_until('nc -z %s 9092' % kafka.dcinfo['NetworkSettings']['IPAddress'])
mongo.start()
nbi.start()
ro_db.start()
ro.start()
wait_until('nc -z %s 9090' % ro.dcinfo['NetworkSettings']['IPAddress'])
lcm.start()

net.ping([d1, zookeeper])
net.ping([d1, kafka])
net.ping([d1, mongo])
net.ping([d1, nbi])
net.ping([d1, ro])
net.ping([d1, ro_db])
net.ping([d1, lcm])

net.stop()
