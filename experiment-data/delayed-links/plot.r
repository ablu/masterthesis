library(tidyverse)
library(gmodels)

plot_width <- 6
plot_height <- 3.5


d = read.csv("delayed-links/merged.csv") %>%
  rename(`RO to DB`=rtt_ro_db_ms, `DB to RO`=rtt_db_ro_ms) %>%
  rename(`Startup`=startup, `Deployment`=deployment, `Deletion`=deletion) %>%
  gather(Var, Value, -delay) %>%
  group_by(Var, delay) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )

start_deploy_delete <- d
start_deploy_delete$Var <- factor(start_deploy_delete$Var, levels=c("Startup", "Deployment", "Deletion"))

width <- 9
pos <- position_dodge(width=width)
ggplot(filter(start_deploy_delete, !is.na(Var)), aes(x=delay, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=pos, width=width) +
  geom_errorbar(position=pos, width=width/2, size=0.2) +
  scale_x_continuous(name="Emulated link delay [ms]", breaks=seq(0, 130, 10)) + 
  scale_y_continuous(name="Mean startup time [s]", breaks=seq(0, 750, 50)) +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_delay.eps', width=plot_width, height=plot_height)

ping <- d
ping$Var <- factor(ping$Var, levels=c("RO to DB", "DB to RO"))

pos <- position_dodge(width=width)
ggplot(filter(ping, !is.na(Var)), aes(x=delay, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=pos, width=width) +
  geom_errorbar(position=pos, width=width/2, size=0.2) +
  scale_x_continuous(name="Emulated link delay [ms]", breaks=seq(0, 130, 10)) + 
  scale_y_continuous(name="Mean ping time [ms]", breaks=seq(0, 500, 50)) +
  labs(fill="") +
  theme_bw()
ggsave('../thesis/figures/plot_delay_ping.eps', width=plot_width, height=plot_height)
