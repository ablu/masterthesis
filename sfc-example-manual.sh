#!/bin/bash

set -xe

openstack port create p1 --network internal
openstack port create p2 --network internal
openstack port create p3 --network internal

openstack server create --port p1 --flavor m1.tiny --image cirros s1
openstack server create --port p2 --flavor m1.tiny --image cirros s2
openstack server create --port p3 --flavor m1.tiny --image cirros s3

wait 60
openstack sfc port pair create --ingress p1 --egress p1 pp1
openstack sfc port pair create --ingress p2 --egress p2 pp2
openstack sfc port pair create --ingress p3 --egress p3 pp3
openstack sfc port pair group create --port-pair pp1 ppg1
openstack sfc port pair group create --port-pair pp2 ppg2
openstack sfc port pair group create --port-pair pp3 ppg3
openstack sfc flow classifier create --logical-source-port p1 fc1
openstack sfc port chain create --port-pair-group ppg2 --port-pair-group ppg3 --flow-classifier fc1 pc1