=====================================
 Enabling DevOps for VNF development
=====================================
:Author: `Erik Schilling <mail@ablu.org>`_
:Date: 2019-02-04


Motivation
==========

What I do
---------

*Telco in a Box: Emulating Full-Stack NFV Deployments*

- Supervised by Manuel Peuster
- Fixed Juju with vim-emu
- Fixes for SFC with vim-emu

. . .

- **Next step:** Exploring how to make prototyping easier

Testing with OpenStack
----------------------
.. image:: figures/architecture.svg
    :width: 100%


Testing with vim-emu
--------------------
.. image:: figures/architecture-vim-emu.svg
    :width: 100%

Current Situation
-----------------
.. image:: figures/isolated-environment.svg
    :width: 100%

Proposal
--------
.. image:: figures/isolated-environment-merged.svg
    :width: 100%

vim-emu (current script)
------------------------
.. code:: python

    self.net = DCNetwork()
    self.dc1 = self.net.addDatacenter("dc1")
    self.dc2 = self.net.addDatacenter("dc2")
    self.net.addLink(self.dc1, self.dc2,
                     cls=TCLink, delay="50ms")
    self.api1 = OpenstackApi("0.0.0.0", 6001)
    self.api1.connect_datacenter(self.dc1)
    self.api2 = OpenstackApi("0.0.0.0", 6002)
    self.api2.connect_datacenter(self.dc2)


vim-emu (sketched extension)
----------------------------
.. code:: python

    self.osm = OSM(api_port=9999, tag="v5.0.2")
    self.osm.connect_datacenter(dc1)
    self.osm.onboard_vnfd("folder_to_vnf/")
    self.osm.onboard_nsd("folder_to_nsd/")
    self.osm.register_vim(openstack_api)
    self.osm.instantiate_ns("test")

. . .

- (could work the same with lower-level components like NBI, LCM, RO, ...)


Comparison
----------
.. class:: columns

    .. class:: column

        .. image:: figures/isolated-environment.svg
            :width: 100%
        - Manual OSM install
        - Allows testing with existing environments

    .. class:: column

        .. image:: figures/isolated-environment-merged.svg
            :width: 100%
        - OSM launched ``within'' vim-emu
        - Can reconfigure internals automatically

Comparison continued...
-----------------------
.. class:: columns

    .. class:: column

        .. image:: figures/isolated-environment.svg
            :width: 100%
        - Testing existing installations is a strong use case

    .. class:: column

        .. image:: figures/isolated-environment-merged.svg
            :width: 100%
        - Automated distributed OSM installs would be simple
. . .

**Existing use cases are not impacted!**

Advantages
----------

- Easier, reproducible testing
- Dead simple CI
  - Even against real OpenStack endpoints
- Emulation of distributed OSM
- Only requires changed to vim-emu
- **Easier to get started with OSM**
