Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
  config.disksize.size = "30GB"


  config.vm.network "private_network", ip: "192.168.50.4"

  config.vm.provider "virtualbox" do |v|
    v.customize ["modifyvm", :id, "--cpuexecutioncap", "75"]
    v.memory = 8000
  end


  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 2376, host: 2376

  config.vm.synced_folder "../../osm/vim-emu", "/vim-emu/"
  config.vm.synced_folder "../../osm/containernet", "/containernet/"
  config.vm.synced_folder "../../osm/LCM", "/LCM/"
  config.vm.synced_folder "../../osm/RO", "/RO/"
  config.vm.synced_folder "../../osm/common", "/common/"

  config.vm.provision "bootstrap", type: "shell", inline: <<-SHELL
  set -xe
  apt update -y
  apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common ryu-bin nmap virtualenv python3-dev
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
  snap install charm --classic
  apt-get update
  apt-get install -y docker-ce python-docker
  mkdir -p /etc/systemd/system/docker.service.d/
  cp /vagrant/docker-override.conf /etc/systemd/system/docker.service.d/override.conf
  systemctl daemon-reload
  systemctl restart docker.service
  docker container run --rm hello-world
  docker swarm init --advertise-addr 192.168.50.4 || echo "swarm already inited"

  apt-get install -y python-dev python-pip python-prettytable flake8 libcurl4-openssl-dev libssl-dev
  pip install pycurl
  pip install git+https://osm.etsi.org/gerrit/osm/osmclient.git
  SHELL
  
  config.vm.provision "vim-emu-install", type: "shell", privileged: false, inline: <<-SHELL
  set -xe
  sudo apt-get install -y ansible python-openstackclient
  cd /containernet/
  sudo util/install.sh
  sudo pip install -e /containernet/

  cd /vim-emu/
  sudo pip install -e /vim-emu/

  echo 'export OS_USERNAME="admin"' >> ~/.bashrc
  echo 'export OS_PASSWORD="nope"' >> ~/.bashrc
  echo 'export OS_PROJECT_NAME="nope"' >> ~/.bashrc
  echo 'export OS_AUTH_URL="http://0.0.0.0:6001"' >> ~/.bashrc
  SHELL

  config.vm.provision "osm-build", type: "shell", inline: <<-SHELL
  set -xe
  git clone https://osm.etsi.org/gerrit/osm/devops.git || echo "already cloned"
  cd devops/docker/Keystone
  make VERBOSE=1 &
  cd -

  (git clone https://osm.etsi.org/gerrit/osm/LCM.git || echo "already cloned") && docker build -f LCM/Dockerfile.local -t opensourcemano/lcm:master LCM/ &

  (git clone https://osm.etsi.org/gerrit/osm/LW-UI.git || echo "already cloned") && docker build -f LW-UI/docker/Dockerfile -t opensourcemano/light-ui:master LW-UI/ &

  (git clone https://osm.etsi.org/gerrit/osm/MON.git || echo "already cloned") &&
  docker build -f MON/docker/Dockerfile -t opensourcemano/mon:master MON/ &

  (git clone https://osm.etsi.org/gerrit/osm/NBI.git || echo "already cloned") &&
  docker build -f NBI/Dockerfile.local -t opensourcemano/nbi:master NBI/ &

  (git clone https://osm.etsi.org/gerrit/osm/POL.git || echo "already cloned") &&
  docker build -f POL/docker/Dockerfile -t opensourcemano/pol:master POL/ &

  (git clone https://osm.etsi.org/gerrit/osm/RO.git || echo "already cloned") &&
  docker build -f RO/docker/Dockerfile-local -t opensourcemano/ro:master RO/ &

  wait

  docker build -t ro-dbg /vagrant/ro-dbg/
  docker build -t lcm-dbg /vagrant/lcm-dbg/
  SHELL

  config.vm.provision "setup-juju", type: "shell", privileged: false, inline: <<-SHELL
  set -xe
  sudo snap install juju --classic
  sudo pip install shyaml
  sudo juju bootstrap --bootstrap-series=xenial localhost osm
  VCA_IP=$(sudo juju show-controller osm|shyaml get-value osm.details.api-endpoints.0 | sed 's/:17070//')
  VCA_PASS=$(sudo cat ~/.local/share/juju/accounts.yaml | shyaml get-value controllers.osm.password)

  cat /vagrant/lcm.env.unpatched > /vagrant/env/lcm.env
  echo "OSMLCM_VCA_HOST=$VCA_IP" >> /vagrant/env/lcm.env
  echo "OSMLCM_VCA_SECRET=$VCA_PASS" >> /vagrant/env/lcm.env
  SHELL
  
  config.vm.provision "restack", type: "shell", privileged: true, inline: <<-SHELL
  set -xe
  docker stack rm osm
  docker system prune -f
  until docker volume ls | grep osm | awk '{print $2}' | xargs -r docker volume rm -f
  do
    sleep 5
  done
  docker stack deploy -c /vagrant/docker-compose.yaml osm
  until osm vim-list
  do
    docker stack ps osm
    sleep 5
  done
  sleep 80 # give other services some time
  MYSQL=$(docker ps --filter "name=osm_ro-db*" --format "{{.ID}}")
  docker exec $MYSQL bash -c "echo 'USE mano_db;ALTER TABLE vim_wim_actions drop item; ALTER TABLE vim_wim_actions ADD item varchar(1024);describe vim_wim_actions' | mysql -u root -pA8l08PRW1YQKHgcUr5TnsxLRi8WmpVMB"
  SHELL

  config.vm.provision "build-charms", type: "shell", privileged: false, inline: <<-SHELL
  set -xe
  export JUJU_REPOSITORY=/vagrant/examples/charms/
  export LAYER_PATH=$JUJU_REPOSITORY/layers
  cd $LAYER_PATH/simple
  charm build || echo "" # charm build returns non-zero exit code, but everything is built

  cd /vagrant/examples/charms/builds
  rm -fr /vagrant/examples/charmed-vnfd/charms/
  mkdir -p /vagrant/examples/charmed-vnfd/charms/
  cp -r simple /vagrant/examples/charmed-vnfd/charms/
  SHELL
  
  config.vm.provision "reboard", type: "shell", privileged: false, inline: <<-SHELL
  set -xe
  /vagrant/reboard.sh
  SHELL
  
  config.vm.provision "priv", type: "shell", privileged: false, inline: <<-SHELL
  set -xe
  /vagrant/priv/*.sh
  SHELL
end
