library(tidyverse)
library(gmodels)

plot_width <- 6
plot_height <- 3.5


d = read.csv("limit/merged.csv") %>%
  gather(Var, Value, -i) %>%
  group_by(Var, i) %>%
  summarise(
    mean=ci(Value)[1],
    low=ci(Value)[2],
    high=ci(Value)[3]
  )

ram <- d
ram$Var <- factor(ram$Var, levels=c("used_ram"))

factor <- 8 * 1024 * 1024 # MB
ram$mean <- ram$mean / factor
ram$low <- ram$low / factor
ram$high <- ram$high / factor
width <- 0.4
position=position_dodge(width=width)
ggplot(filter(ram, !is.na(Var)), aes(x=i, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name="Number of NSs", breaks=seq(0, 14, 1)) + 
  scale_y_continuous(name="Mean RAM usage [MiB]", breaks=seq(0, 750, 100)) +
  labs(fill="") +
  theme_bw() +
  theme(legend.position = "none")
ggsave('../thesis/figures/plot_limit_ram.eps', width=plot_width, height=plot_height)


cpu <- d
cpu$Var <- factor(cpu$Var, levels=c("cpu_usage"))

position=position_dodge(width=width)
ggplot(filter(cpu, !is.na(Var)), aes(x=i, y=mean, ymin=low, ymax=high, fill=Var)) +
  geom_col(position=position, width=width) +
  geom_errorbar(position=position, width=width/2, size=0.2) +
  scale_x_continuous(name="Number of NSs", breaks=seq(0, 14, 1)) + 
  scale_y_continuous(name="Mean CPU usage [%]") +
  labs(fill="") +
  theme_bw() +
  theme(legend.position = "none")
ggsave('../thesis/figures/plot_limit_cpu.eps', width=plot_width, height=plot_height)
