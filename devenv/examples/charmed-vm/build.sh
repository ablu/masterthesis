#!/bin/bash
set -xe

virt-builder centos-7.6 \
    --root-password password:test \
    --run-command "systemctl disable firewalld" \
    --run-command "systemctl mask --now firewalld || exit 0" \
    --selinux-relabel \
    -o charmed.img

openstack image create --file charmed.img charmed
